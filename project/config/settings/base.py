"""
Django settings for course_creator project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

import os
from django.utils.translation import ugettext_lazy as _

import ldap
from django_auth_ldap.config import LDAPSearch, GroupOfNamesType

import mdx_oembed
import oembed


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = os.path.join(BASE_DIR, '../../')
APP_DIR = os.path.join(BASE_DIR, '../')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'z17*+9#gpdd#_kssvfw=i*v4^(8#7p#nwgp)thew)$2u9+dv)c'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [
    # '.example.com',  # Allow domain and subdomains
    # '.example.com.',  # Also allow FQDN and subdomains
    '*',
]

INTERNAL_IPS = [
    '127.0.0.1',
]

# Configure sites framework
# See https://docs.djangoproject.com/en/1.10/ref/settings/#sites
SITE_ID = 1

DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # flatpages
    'django.contrib.sites',
    'django.contrib.flatpages',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin
    # translation of db content - needs to be above 'django.contrib.admin'
    'modeltranslation',
    'django.contrib.admin',
)

THIRD_PARTY_APPS = (
    # Third party Django apps
    'simple_history',  # Keep track of data changes
    'crispy_forms',  # Form layouts
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'django_extensions',
    'markdownx',
    'rules.apps.AutodiscoverRulesConfig',  # find rules.py automatically
    'django_select2',
)

LOCAL_APPS = (
    # Your stuff: custom apps go here
    'users.apps.UsersConfig',
    'lms_base',
    'lms_tales',
    'lms_nahtkurs',
    'lms_opos',
)


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
# Application definition

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
]

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, '../templates'),
        ],
        'OPTIONS': {
            # See:
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See:
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'de'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


LANGUAGES = (
    ('de', _('Deutsch')),
    ('en', _('Englisch')),
)

LOCALE_PATHS = [
    str(os.path.join(BASE_DIR, '../locale')),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = str(os.path.join(PROJECT_DIR, 'staticfiles'))

STATIC_URL = '/static/'

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "../static"),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

MEDIA_URL = '/media/'

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS

MEDIA_ROOT = os.path.join(BASE_DIR, "../media")

ADMINS = (
    ("""admin""", 'contact-nmc@unibas.ch'),
)

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# Custom user app defaults
# ------------------------------------------------------------------------------
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'users:redirect'
LOGIN_URL = 'account_login'

# Modeltranslation configuration
# ------------------------------------------------------------------------------
MODELTRANSLATION_DEFAULT_LANGUAGE = 'de'
MODELTRANSLATION_FALLBACK_LANGUAGES = ('de', 'en',)


# Configure OEmbed endpoints
# ------------------------------------------------------------------------------
ENDPOINTS = mdx_oembed.endpoints.DEFAULT_ENDPOINTS
SOUNDCLOUD = oembed.OEmbedEndpoint('https://soundcloud.com/oembed', [
    'https?://soundcloud.com/*',
])
ENDPOINTS.append(SOUNDCLOUD)

# Markdonx configuraiton
# ------------------------------------------------------------------------------
MARKDOWNX_MARKDOWNIFY_FUNCTION = 'lms_base.utils.markdownify'

MARKDOWNX_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra',
    'markdown.extensions.smarty',
    'oembed',
    'markdown.extensions.fenced_code',
    'markdown.extensions.codehilite',
]

MARKDOWNX_IMAGE_MAX_SIZE = {
    'size': (1920, 1080),
    'quality': 90
}

MARKDOWNX_MARKDOWN_EXTENSION_CONFIGS = {
    'oembed': {
        'allowed_endpoints': ENDPOINTS
    }
}

# Django crispy_forms configuration
# ------------------------------------------------------------------------------
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# LDAP configuration
# ------------------------------------------------------------------------------
# Baseline configuration.
AUTH_LDAP_SERVER_URI = "ldaps://its-adproxy.its.unibas.ch:636"

AUTH_LDAP_BIND_DN = (
    "CN=nmc-s-binduser - d-nmc-bindus,OU=LOST_AND_FOUND,OU=FROM_IDM,"
    "DC=unibasel,DC=ads,DC=unibas,DC=ch"
)

AUTH_LDAP_BIND_PASSWORD = "sC97SggBq"

AUTH_LDAP_USER_SEARCH = LDAPSearch(
    "DC=unibasel,DC=ads,DC=unibas,DC=ch",
    ldap.SCOPE_SUBTREE, "(uid=%(user)s)"
)

# Set up the basic group parameters.
AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    (
        "CN=NMC-user,OU=DistributionGroups,OU=FROM_IDM,DC=unibasel,"
        "DC=ads,DC=unibas,DC=ch"
    ),
    ldap.SCOPE_SUBTREE, "(objectClass=person)"
)

AUTH_LDAP_GROUP_TYPE = GroupOfNamesType(name_attr="cn")

AUTH_LDAP_REQUIRE_GROUP = (
    "CN=NMC-user,OU=DistributionGroups,OU=FROM_IDM,DC=unibasel,"
    "DC=ads,DC=unibas,DC=ch"
)
# Populate the Django user from the LDAP directory.
AUTH_LDAP_USER_ATTR_MAP = {
    "email": "mail"
}

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_active": (
        "CN=NMC-user,OU=DistributionGroups,OU=FROM_IDM,DC=unibasel,DC=ads,"
        "DC=unibas,DC=ch"
    ),
    "is_staff": (
        "CN=NMC-user,OU=DistributionGroups,OU=FROM_IDM,DC=unibasel,DC=ads,"
        "DC=unibas,DC=ch"
    ),
    # "is_superuser": "cn=superuser,ou=django,ou=groups,dc=example,dc=com"
}

# This is the default, but I like to be explicit.
AUTH_LDAP_ALWAYS_UPDATE_USER = True

# Cache group memberships for an hour to minimize LDAP traffic
AUTH_LDAP_CACHE_GROUPS = True
AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600

# Keep ModelBackend around for per-user permissions and maybe a local
# superuser.
AUTHENTICATION_BACKENDS = (
    'django_auth_ldap.backend.LDAPBackend',
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)
