# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from .base import *  # noqa: F403

THIRD_PARTY_APPS = (
    'debug_toolbar',
)

DEBUG = True

INSTALLED_APPS = INSTALLED_APPS + THIRD_PARTY_APPS  # noqa: F405

MIDDLEWARE.append(  # noqa: F405
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)
