# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
"""
WSGI config for course_creator project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import sys
import time
import traceback
import signal
sys.path.insert(
    0, '/srv/www/vhosts/tales.nmc.unibas.ch/files/cgi-bin/env'
)
from django.core.wsgi import get_wsgi_application  # noqa: E402

# Add additional paths
sys.path.append('/srv/www/vhosts/tales.nmc.unibas.ch/files/cgi-bin')
sys.path.append('/srv/www/vhosts/tales.nmc.unibas.ch/files/cgi-bin/nahtkurs')
sys.path.append(
        '/srv/www/vhosts/tales.nmc.unibas.ch/files/cgi-bin/site-packages'
        )

os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings.nahtkurs.production"

try:
    application = get_wsgi_application()
except RuntimeError as re:
    print(re)
    print('handling WSGI exception')
    # Error loading applications
    if 'mod_wsgi' in sys.modules:
        traceback.print_exc()
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(2.5)
