# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from ..base import *  # noqa: F403,F401

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tales_nmc_unibas_ch_01',
        'USER': 'tales_nmc_unibas_ch_01',
        'PASSWORD': 'eiyahj3choo2eiZa',
        "HOST": 'its-web-sql-008.its.unibas.ch',
        "PORT": "3306",
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    },
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls_nahtkurs'
# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = "/srv/www/vhosts/tales.nmc.unibas.ch/files/html/static/"
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/
STATICFILES_DIRS = [
    # "os.path.join(BASE_DIR, "static"),
    # '/srv/www/vhosts/nmc.unibas.ch/files/html/static/',
]

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = "/srv/www/vhosts/tales.nmc.unibas.ch/files/html/media/nahtkurs/"

MEDIA_URL = '/media/nahtkurs/'

MARKDOWNX_URLS_PATH = '/nahtkurs/markdownx/markdownify/'
MARKDOWNX_UPLOAD_URLS_PATH = '/nahtkurs/markdownx/upload/'
MARKDOWNX_MEDIA_PATH = 'markdownx/'
