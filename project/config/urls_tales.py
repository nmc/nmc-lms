# -*- coding: utf-8 -*-

from django.conf import settings

from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

from django.views import defaults as default_views

from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

from lms_tales import views

# Add the files which shouldn't be internationalized
urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    url(
        r'^$',
        TemplateView.as_view(template_name='pages/home.html'),
        name='home'
    ),

    url(settings.ADMIN_URL, admin.site.urls),

    # User management
    url(r'^users/', include('users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    # Markdown processor
    url(r'^markdownx/', include('markdownx.urls')),
]

urlpatterns += i18n_patterns(
    # Examples:
    # url(r'^$', 'nmc_lms.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Notice the expression does not end in $,
    # that happens at the myapp/url.py level
    # url(r'^tales/',include('lms_tales.urls', namespace='tales')),
    url(r'^', include('lms_tales.urls', namespace='tales')),
    url(r'^courselist/',
        views.CourseList.as_view(template_name='course_list.html'),
                           name='courselist'
                           ),
    url(r'^krank-ohne-ursache/',
        RedirectView.as_view(url=reverse_lazy('tales:course',
                             kwargs={'slug': 'krank-ohne-ursache', 'pk': '2'}
                                            )
                             ),
        name='redirect_krank-ohne-ursache'),
    url(r'^erasmus/',
        RedirectView.as_view(url=reverse_lazy('tales:course',
                             kwargs={'slug': 'erasmus-von-rotterdam', 'pk': '3'}  # NOQA
                                            )
                             ),
        name='redirect_erasmus'),
    url(r'^linear-algebra-ii/',
        RedirectView.as_view(url=reverse_lazy('tales:course',
                             kwargs={'slug': 'linear-algebra-ii', 'pk': '4'}
                                            )
                             ),
        name='redirect_linear-algebra-ii'),
    url(r'^conducting-psychological-research/',
        RedirectView.as_view(
            url=reverse_lazy(
                'tales:course',
                kwargs={'slug': 'conducting-psychological-research', 'pk': '6'}
                            )
                            ),
        name='conducting-psychological-research'),
    url(r'^entrepreneurship-in-nonprofits/',
        RedirectView.as_view(url=reverse_lazy('tales:course',
                             kwargs={'slug': 'entrepreneurship-in-nonprofits', 'pk': '10'}  # NOQA
                                            )
                             ),
        name='redirect_entrepreneurship-in-nonprofits'),
    url(r'^base/', include('lms_base.urls', namespace='base')),
    url(r'^select2/', include('django_select2.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
