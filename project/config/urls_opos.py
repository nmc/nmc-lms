# -*- coding: utf-8 -*-

from django.conf import settings

from django.conf.urls import include, url
from django.contrib import admin

from django.views import defaults as default_views

from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

# Add the files which shouldn't be internationalized
urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),
    # User management
    url(r'^users/', include('users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),
    # Markdown processor
    url(r'^markdownx/', include('markdownx.urls')),
]

urlpatterns += i18n_patterns(
    url(r'^', include('lms_opos.urls', namespace='opos')),
    url(r'^base/', include('lms_base.urls', namespace='base')),
    url(r'^select2/', include('django_select2.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
