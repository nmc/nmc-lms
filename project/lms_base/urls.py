# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django.views.generic.base import TemplateView

urlpatterns = [
    # markdown tutorial
    url(
        r'^markdown$',
        TemplateView.as_view(template_name='markdown.html'),
        name='markdown'
    ),
    url(
        r'^impressum$',
        TemplateView.as_view(template_name='impressum.html',),
        name='impressum'
    ),
    url(
        r'^about$',
        TemplateView.as_view(template_name='about.html',),
        name='about'
    ),
    url(
        r'^help$',
        TemplateView.as_view(template_name='help.html',),
        name='help'
    ),
]
