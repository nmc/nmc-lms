/* global $, document, window */
/* eslint prefer-template: "error" */
/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* jshint esversion: 6 */


$(document).ready(function () {
  $('.nav-close-button, #main-content').on('click', function () {
    $('#sidebar-nav').removeClass('open');
    $('#sidebar-toggle').removeClass('open');
    $('.subcollapse').collapse('hide');
  });
  $('.nav-open-button').on('click', function () {
    $('#sidebar-nav').addClass('open');
    $('#sidebar-toggle').addClass('open');
    $('#sidebar-nav .active').parent().children('.subcollapse').collapse('show');
  });
  $('#sidebar-nav .panel').on('click', function () {
    $('#sidebar-nav').addClass('open');
    $('#sidebar-toggle').addClass('open');
  });
  $('#glossar-modal').on('shown.bs.modal', function (event) {
    const delayInMilliseconds = 700;
    setTimeout(function () {
      window.location = `#${event.relatedTarget.title}`;
    }, delayInMilliseconds);
  });
});
