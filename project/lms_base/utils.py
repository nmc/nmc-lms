# -*- coding: utf-8 -*-
import markdown
from . import markdowm_img_captions

from config.settings.base import MARKDOWNX_MARKDOWN_EXTENSIONS

MARKDOWNX_MARKDOWN_EXTENSIONS.append(markdowm_img_captions.makeExtension())


def markdownify(content):
    return markdown.markdown(
            content,
            extensions=MARKDOWNX_MARKDOWN_EXTENSIONS
            )
