from django import template

from markdownx import utils

register = template.Library()


@register.filter
@register.simple_tag
def markdownify(text):
    return utils.markdownify(text)


@register.simple_tag
def get_verbose_name(instance):
    """
    Returns verbose_name for a field.
    """
    return instance.model._meta.verbose_name.title()
