# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.db import models

from django.db.models import Q
from django.contrib.auth.models import Group
from django.utils.encoding import python_2_unicode_compatible

from markdownx.models import MarkdownxField
from uuslug import uuslug


class NextPreviousQuerySet(models.QuerySet):

    def get_next_previous(self, is_next, instance):
        first_level_order, second_level_order = self.model._meta.ordering
        op = 'gt' if is_next else 'lt'

        # get all in current chapter/course
        section_query = Q(
                **{'%s' % first_level_order: getattr(
                    instance, first_level_order
                    )}
                )
        # get all previous/following chapters/courses
        order_query = Q(
                **{'order__%s' % op: getattr(instance, second_level_order)}
                )
        qs = self.filter(section_query).filter(order_query)

        if is_next:
            return qs.first()
        return qs.last()


# directs to /{coursename}/
def course_directory_path(instance, filename):
    return '{0}/{1}'.format(instance.slug, filename)


# directs to /{coursename}/{chapter}/{step}/
def step_directory_path(instance, filename):
    return '{0}/{1}/{2}/{3}'.format(
        instance.chapter.course.slug,
        instance.chapter.slug,
        instance.slug,
        filename)


@python_2_unicode_compatible
class StepMixin(models.Model):
    TYPE_AUDIO = 'TA'
    TYPE_VIDEO = 'TV'
    TYPE_TEXT = 'TT'
    TYPE_MIXED = 'TM'
    TYPE_QUIZ = 'TQ'
    TYPE_CHOICES = (
        (TYPE_AUDIO, 'Audio'),
        (TYPE_VIDEO, 'Video'),
        (TYPE_TEXT, 'Text'),
        (TYPE_MIXED, 'Mixed'),
        (TYPE_QUIZ, 'Quiz'),
    )

    title = models.CharField(max_length=80)
    subtitle = models.CharField(max_length=80, blank=True)
    title_image = models.ImageField(upload_to=step_directory_path,
                                    max_length=255,
                                    blank=True)
    slug = models.CharField(max_length=200, editable=False)
    content = MarkdownxField(
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )
    copyright = MarkdownxField(
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )
    step_type = models.CharField(
        max_length=2,
        choices=TYPE_CHOICES,
        default=TYPE_TEXT,
    )
    duration = models.CharField(
            max_length=70,
            blank=True,
            help_text=(
                "Die Dauer kann in Minuten angegeben werden. "
                "Beispiel: '10 Minuten'"
                )
            )
    order = models.IntegerField()
    objects = models.Manager()
    next_previous_manager = NextPreviousQuerySet.as_manager()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(StepMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True
        unique_together = (('order', 'chapter'),)
        ordering = ['chapter', 'order', ]

    def _get_neighbour_step(self, is_next):
        # get neighbour step in same chapter
        step = self.__class__.next_previous_manager.get_next_previous(
                is_next, self
                )

        if step:
            return step

        # no neighbour in this chapter, get first/last step of neighbor chapter
        neighbour_chapter = (
                self.chapter.__class__.next_previous_manager.get_next_previous(
                    is_next, self.chapter
                    )
                )

        qs = self.__class__.objects.all().filter(chapter=neighbour_chapter)

        if is_next:
            return qs.first()
        return qs.last()

    def get_next(self):
        return self._get_neighbour_step(is_next=True)

    def get_previous(self):
        return self._get_neighbour_step(is_next=False)

    def type_verbose(self):
        return dict(self.TYPE_CHOICES)[self.step_type]


@python_2_unicode_compatible
class ChapterMixin(models.Model):
    title = models.CharField(max_length=80)
    slug = models.CharField(max_length=200, editable=False)
    order = models.IntegerField()
    objects = models.Manager()
    next_previous_manager = NextPreviousQuerySet.as_manager()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(ChapterMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True
        unique_together = (('order', 'course'),)
        ordering = ['course', 'order', ]

    def get_next(self):
        return self.__class__.next_previous_manager.get_next_previous(
                True, self
                )

    def get_previous(self):
        return self.__class__.next_previous_manager.get_next_previous(
                False, self
                )


@python_2_unicode_compatible
class CourseMixin(models.Model):
    title = models.CharField(max_length=80)
    subtitle = models.CharField(max_length=80, blank=True)
    title_image = models.ImageField(upload_to=course_directory_path,
                                    max_length=255,
                                    default='default.jpg')
    slug = models.CharField(max_length=200, editable=False)
    content = MarkdownxField(
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )
    copyright = MarkdownxField(
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )
    owner = models.ManyToManyField(Group)
    course_contact = MarkdownxField(
            blank=True,
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(CourseMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class GlossaryMixin(models.Model):
    title = models.CharField(max_length=80)
    objects = models.Manager()

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class GlossaryItemMixin(models.Model):
    title = models.CharField(max_length=80)
    content = MarkdownxField(
            help_text="Dieses Feld kannst Du mit Markdown formatieren."
            )
    links = models.ManyToManyField('GlossaryItemMixin',
                                   blank=True)
    images = models.ImageField(upload_to=course_directory_path,
                               max_length=255,
                               blank=True)
    references = MarkdownxField(
            help_text="Jeden Markdown Link einzeln mit {.referenz-link} \
            abschliessen. \
            Referenzen ohne verlinkung mit {.referenz-text} abschliessen.",
            blank=True
            )
    objects = models.Manager()

    def __str__(self):
        return self.title
