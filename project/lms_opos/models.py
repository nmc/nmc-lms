# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.db import models
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible

from lms_base.models import StepMixin, ChapterMixin, CourseMixin, GlossaryItemMixin  # NOQA


class OposStep(StepMixin):  # noqa 405
    chapter = models.ForeignKey(
            'OposChapter', null=True, related_name='steps'
            )

    def get_course(self):
        return self.chapter.course

    def get_absolute_url(self):
        return reverse('opos:step', args=[str(self.chapter.course.slug),
                                          str(self.chapter.course.id),
                                          str(self.chapter.slug),
                                          str(self.chapter.id),
                                          str(self.slug),
                                          str(self.id), ])

    def get_step_number(self):
        chapter_list = list(self.chapter.course.chapters.all()
                            .order_by('order'))
        chapter_num = str(chapter_list.index(self.chapter) + 1)
        step_list = list(self.chapter.steps.all().order_by('order'))
        step_num = str(step_list.index(self) + 1)
        index = chapter_num + "." + step_num
        return index

    def is_first_step_in_chapter(self):
        step_list = list(self.chapter.steps.all().order_by('order'))
        return step_list[0] == self

    def get_first_step_in_chapter(self):
        step_list = list(self.chapter.steps.all().order_by('order'))
        return step_list[0]

    def save(self, *args, **kwargs):
        if not self.chapter.steps.all():
            self.order = 0
        if self.order is None:
            self.order = max(step.order for step in
                             self.chapter.steps.all()) + 1
        super(OposStep, self).save(*args, **kwargs)


def step_directory_path(instance, filename):
    return '{0}/{1}/{2}/{3}'.format(
        instance.step.chapter.course.slug,
        instance.step.chapter.slug,
        instance.step.slug,
        filename)


@python_2_unicode_compatible
class OposStepFile(models.Model):
    title = models.CharField(max_length=70)
    step = models.ForeignKey('OposStep', related_name='files')
    file = models.FileField(upload_to=step_directory_path, max_length=255)

    def __str__(self):
        return self.title


class OposChapter(ChapterMixin):  # noqa 405

    course = models.ForeignKey(
            'OposCourse', null=True, related_name='chapters'
            )

    def get_course(self):
        return self.course

    def get_absolute_url(self):
        return reverse('opos:chapter', args=[str(self.course.slug),
                                             str(self.course.id),
                                             str(self.slug),
                                             str(self.id), ])

    def save(self, *args, **kwargs):
        if not self.course.chapters.all():
            self.order = 0
        if self.order is None:
            self.order = max(chapter.order for chapter in
                             self.course.chapters.all()) + 1
        super(OposChapter, self).save(*args, **kwargs)


class OposCourse(CourseMixin):  # noqa 405

    def get_course(self):
        return self

    def get_absolute_url(self):
        return reverse('opos:course', args=[str(self.slug),
                                            str(self.id), ])


class OposGlossaryItem(GlossaryItemMixin):  # noqa 405

    def __init__(self, *args, **kwargs):
        for f in self._meta.fields:
            if f.attname == "images":
                f.upload_to = "glossar"
        super(OposGlossaryItem, self).__init__(*args, **kwargs)
