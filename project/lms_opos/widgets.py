# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms

from django.template.loader import get_template
from markdownx.widgets import MarkdownxWidget
from markdownx.settings import (
    MARKDOWNX_EDITOR_RESIZABLE,
    MARKDOWNX_URLS_PATH,
    MARKDOWNX_UPLOAD_URLS_PATH,
)


class CustomisableMarkdownxWidget(MarkdownxWidget):
    def render(self, name, value, attrs=None):
        attrs = self.build_attrs(attrs, name=name)
        if 'class' in attrs:
            attrs['class'] += ' markdownx-editor'
        else:
            attrs.update({'class': 'markdownx-editor'})
        attrs['data-markdownx-editor-resizable'] = MARKDOWNX_EDITOR_RESIZABLE
        attrs['data-markdownx-urls-path'] = MARKDOWNX_URLS_PATH
        attrs['data-markdownx-upload-urls-path'] = MARKDOWNX_UPLOAD_URLS_PATH

        widget = forms.Textarea.render(self, name, value, attrs)

        template = get_template('markdownx/widget.html')
        return template.render({
            'markdownx_editor': widget,
            'name': name,
            'value': value,
            'attrs': attrs,
        })
