# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.forms import ModelForm, widgets
from django.utils.datastructures import MultiValueDict  # NOQA

from extra_views import InlineFormSet

from lms_opos import widgets, models  # NOQA


class CourseForm(ModelForm):
    class Meta:
        model = models.OposCourse
        exclude = ['slug', 'owner', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'content_de': widgets.CustomisableMarkdownxWidget(),
            'content_en': widgets.CustomisableMarkdownxWidget(),
            'author': widgets.CustomisableMarkdownxWidget(),
            'author_de': widgets.CustomisableMarkdownxWidget(),
            'author_en': widgets.CustomisableMarkdownxWidget(),
            'copyright': widgets.CustomisableMarkdownxWidget(),
            'copyright_de': widgets.CustomisableMarkdownxWidget(),
            'copyright_en': widgets.CustomisableMarkdownxWidget(),
            'course_contact': widgets.CustomisableMarkdownxWidget(),
        }


class UpdateCourseForm(CourseForm):
    title_image = forms.ImageField(required=False)


class ChapterForm(ModelForm):
    class Meta:
        model = models.OposChapter
        exclude = ['slug', 'order', 'course', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
        }


class StepForm(ModelForm):
    class Meta:
        model = models.OposStep
        exclude = ['slug',
                   'order', 'chapter', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'content_de': widgets.CustomisableMarkdownxWidget(),
            'content_en': widgets.CustomisableMarkdownxWidget(),
            'copyright': widgets.CustomisableMarkdownxWidget(),
            'copyright_de': widgets.CustomisableMarkdownxWidget(),
            'copyright_en': widgets.CustomisableMarkdownxWidget(),
        }


class GlossaryItemForm(ModelForm):
    class Meta:
        model = models.OposGlossaryItem
        exclude = []
        # fields = ['title', 'content', 'links', 'images', 'references']

        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'references': widgets.CustomisableMarkdownxWidget(),
            'links': forms.SelectMultiple(),
        }


class FilesInline(InlineFormSet):
    model = models.OposStepFile
    fields = ['title', 'file']
    extra = 1
