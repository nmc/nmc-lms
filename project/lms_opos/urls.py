# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings

from django.conf.urls import url
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView

from lms_opos import views


static_urls = [
    # markdown tutorial
    url(
        r'^markdown$',
        TemplateView.as_view(template_name='markdown.html'),
        name='markdown'
    ),
    url(
        r'^impressum$',
        TemplateView.as_view(template_name='impressum.html',),
        name='impressum'
    ),
    url(
        r'^about$',
        TemplateView.as_view(template_name='pages/opos_about.html',),
        name='about'
    ),
    url(
        r'^help$',
        TemplateView.as_view(template_name='help.html',),
        name='help'
    ),
]

course_urls = [
    # course list
    url(
        r'^$',
        views.CourseList.as_view(template_name='opos_course_list.html'),
        name='course_list'
    ),
    url(
        r'^course/add$',
        views.CourseCreate.as_view(),
        name='course_add'
    ),
    url(
        r'^course/edit/(?P<pk>\d+)$',
        never_cache(views.CourseUpdate.as_view()),
        name='course_edit'
    ),
    url(
        r'^course/delete/(?P<pk>\d+)$',
        views.CourseDelete.as_view(),
        name='course_delete'
    ),
    # course overview / chapter list
    url(
        r'^(?P<slug>[\w-]+)-(?P<pk>\d+)/$',
        views.CourseDetails.as_view(
            template_name='opos_course_home.html',
            ),
        name='course'
    ),
    url(
        r'^(?P<slug>[\w-]+)-(?P<pk>\d+)/kontakt$',
        views.CourseDetails.as_view(
            template_name='opos_kontakt.html',
            ),
        name='kontakt'
    ),
]

chapter_urls = [
    url(
        r'^chapter/add/(?P<course_pk>\d+)$',
        views.ChapterCreate.as_view(),
        name='chapter_add'
    ),
    url(
        r'^chapter/edit/(?P<pk>\d+)$',
        never_cache(views.ChapterUpdate.as_view()),
        name='chapter_edit'
    ),
    url(
        r'^chapter/delete/(?P<pk>\d+)$',
        views.ChapterDelete.as_view(),
        name='chapter_delete'
    ),
    # chapter overview / step list
    url(
        r'^(?P<course_slug>[\w-]+)-(?P<course_pk>\d+)/'
        r'(?P<slug>[\w-]+)-(?P<pk>\d+)/$',
        views.ChapterDetails.as_view(
            template_name='opos_chapter_home.html',
            ),
        name='chapter'
    ),
    url(
        r'^chapter_order$',
        views.chapter_update_order,
        name='chapter_update_order'
    ),
]

step_urls = [
    url(
        r'^step/add/(?P<chapter_pk>\d+)$',
        views.StepCreate.as_view(),
        name='step_add'
    ),
    url(
        r'^step/edit/(?P<pk>\d+)$',
        never_cache(views.StepUpdate.as_view()),
        name='step_edit'
    ),
    url(
        r'^step/delete/(?P<pk>\d+)$',
        views.StepDelete.as_view(),
        name='step_delete'
    ),
    # step
    url(
        r'^(?P<course_slug>[\w-]+)-(?P<course_pk>\d+)/'
        r'(?P<chapter_slug>[\w-]+)-(?P<chapter_pk>\d+)/'
        r'(?P<slug>[\w-]+)-(?P<pk>\d+)$',
        views.StepDetails.as_view(
            template_name='opos_step_home.html',
            ),
        name='step'
    ),
    url(
        r'^step_order$',
        views.step_update_order,
        name='step_update_order',
    ),
]

glossary_urls = [
    url(
        r'^glossar$',
        views.GlossaryView.as_view(template_name='opos_glossar.html',),
        name='glossar'
    ),
    url(
        r'^glossary/add/$',
        views.OposGlossaryItemCreate.as_view(),
        name='glossaryItem_add'
    ),
    url(
        r'^glossary/edit/(?P<pk>\d+)$',
        never_cache(views.OposGlossaryItemUpdate.as_view()),
        name='glossaryItem_edit'
    ),
    url(
        r'^glossary/delete/(?P<pk>\d+)$',
        views.OposGlossaryItemDelete.as_view(),
        name='glossaryItem_delete'
    ),
]


urlpatterns = static_urls + course_urls + chapter_urls + step_urls + glossary_urls  # NOQA

if settings.DEBUG:
    urlpatterns += [
        url(r'^404/$', views.NotFoundView.as_view()),
    ]
