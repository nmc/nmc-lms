# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.edit import (CreateView, DeleteView, UpdateView)
from django.core.urlresolvers import reverse_lazy, reverse
from django import shortcuts
from django import http
from django.contrib.auth.decorators import login_required
from rules.contrib.views import PermissionRequiredMixin

from collections import OrderedDict

from extra_views import CreateWithInlinesView, UpdateWithInlinesView

from lms_opos import models, forms


ascii_letters = "#/=\"\'@$-_<>*(!§°0123456789aàäAÄbBcCdDeéèEfFgGhHiIjJkKlLmMnNoöOÖpPqQrRsStTuüUÜvVwWxXyYzZ"  # NOQA


# Course views
class CourseDetails(DetailView):
    model = models.OposCourse

    def get(self, request, *args, **kwargs):
        response = super(CourseDetails, self).get(request, *args, **kwargs)
        if (self.object.slug != self.kwargs['slug']):
            return shortcuts.redirect(self.object, permanent=True)
        return response


class CourseList(ListView):
    model = models.OposCourse

    def get_context_data(self, **kwargs):
        context = super(CourseList, self).get_context_data(**kwargs)
        context['oposglossaryitem_list'] = {}
        context['hasGlossary'] = False
        for letter in ascii_letters:
            context['oposglossaryitem_list'][letter] = models.OposGlossaryItem.objects.all().filter(title__startswith=letter)  # NOQA
        if (len(context['oposglossaryitem_list']) > 0):
                context['hasGlossary'] = True

        return context


class CourseCreate(PermissionRequiredMixin, CreateView):
    model = models.OposCourse
    success_url = reverse_lazy('opos:course_list')
    success_message = "%(name)s was created successfully"
    form_class = forms.CourseForm
    template_name = 'add.html'
    permission_required = 'lms_opos.add_oposcourse'


class CourseUpdate(PermissionRequiredMixin, UpdateView):
    model = models.OposCourse
    success_url = reverse_lazy('opos:course_list')
    success_message = "%(name)s was updated successfully"
    form_class = forms.UpdateCourseForm
    template_name = 'add.html'
    permission_required = 'lms_opos.change_oposcourse'


class CourseDelete(PermissionRequiredMixin, DeleteView):
    model = models.OposCourse
    success_url = reverse_lazy('opos:course_list')
    success_message = "%(name)s was deleted successfully"
    form_class = forms.CourseForm
    template_name = 'delete.html'
    permission_required = 'lms_opos.delete_oposcourse'


# Chapter views
class ChapterDetails(DetailView):
    model = models.OposChapter

    def get(self, request, *args, **kwargs):
        response = super(ChapterDetails, self).get(request, *args, **kwargs)
        if (
            self.object.slug != self.kwargs['slug'] or
            self.object.course.slug != self.kwargs['course_slug'] or
            self.object.course.pk != int(self.kwargs['course_pk'])
        ):
            return shortcuts.redirect(self.object, permanent=True)
        return response


class ChapterList(ListView):
    model = models.OposChapter


class ChapterCreate(PermissionRequiredMixin, CreateView):
    model = models.OposChapter
    form_class = forms.ChapterForm
    template_name = 'add.html'
    success_message = "%(name)s was created successfully"
    permission_required = 'lms_opos.add_oposchapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterCreate, self).get_context_data(**kwargs)
        context['course'] = models.OposCourse.objects.get(
                pk=self.kwargs['course_pk']
                )
        return context

    def form_valid(self, form):
        course = shortcuts.get_object_or_404(models.OposCourse,
                                             id=self.kwargs['course_pk'])
        form.instance.course = course
        form.instance.save()
        response = super(ChapterCreate, self).form_valid(form)
        return response

    def get_success_url(self):
        return reverse('opos:course',
                       kwargs={'slug': 'some',
                               'pk': self.kwargs['course_pk']})

    def get_permission_object(self):
        return models.OposCourse.objects.get(pk=self.kwargs['course_pk'])


class ChapterUpdate(PermissionRequiredMixin, UpdateView):
    model = models.OposChapter
    form_class = forms.ChapterForm
    template_name = 'add.html'
    success_message = "%(name)s was updated successfully"
    permission_required = 'lms_opos.change_oposchapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterUpdate, self).get_context_data(**kwargs)
        context['course'] = self.object.course
        return context

    def get_success_url(self):
        return reverse('opos:course',
                       kwargs={'slug': self.object.course.slug,
                               'pk': self.object.course.pk})

    def get_permission_object(self):
        return self.get_object().course


class ChapterDelete(PermissionRequiredMixin, DeleteView):
    model = models.OposChapter
    form_class = forms.ChapterForm
    template_name = 'delete.html'
    success_message = "%(name)s was deleted successfully"
    permission_required = 'lms_opos.delete_oposchapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterDelete, self).get_context_data(**kwargs)
        context['course'] = self.object.course
        return context

    def get_success_url(self):
        return reverse('opos:course',
                       kwargs={'slug': self.object.course.slug,
                               'pk': self.object.course.pk})

    def get_permission_object(self):
        return self.get_object().course


def chapter_update_order(request):
    if request.POST:
        course_id = request.POST.get('pk')
        chapter_order = request.POST.getlist('order[]')
        course = models.OposCourse.objects.get(pk=course_id)
        # Get a list of chapters
        chapters = []
        for step_id in chapter_order:
            chapter = models.OposChapter.objects.get(pk=step_id,
                                                     course=course)
            chapters.append(chapter)
        # find the max value of order
        max_order = max(chapter.order for chapter in chapters) + 1
        # avoid possible colissions
        for chapter in chapters:
            chapter.order = chapter.order + max_order
            chapter.save()
        # set the new chapter order
        for i, chapter in enumerate(chapters):
            chapter.order = i
            chapter.save()
    return http.HttpResponse('o.k.')


# Step views
class StepDetails(DetailView):
    model = models.OposStep

    def get(self, request, *args, **kwargs):
        response = super(StepDetails, self).get(request, *args, **kwargs)
        if (
            self.object.slug != self.kwargs['slug'] or
            self.object.chapter.slug != self.kwargs['chapter_slug'] or
            self.object.chapter.pk != int(self.kwargs['chapter_pk']) or
            self.object.chapter.course.slug != self.kwargs['course_slug'] or
            self.object.chapter.course.pk != int(self.kwargs['course_pk'])
        ):
            return shortcuts.redirect(self.object, permanent=True)
        return response


class StepList(ListView):
    model = models.OposStep


class StepCreate(PermissionRequiredMixin, CreateWithInlinesView):
    model = models.OposStep
    form_class = forms.StepForm
    inlines = [forms.FilesInline]
    template_name = 'add.html'
    success_message = "%(title)s was created successfully"
    permission_required = 'lms_opos.add_oposstep'

    def get_context_data(self, **kwargs):
        context = super(StepCreate, self).get_context_data(**kwargs)
        context['course'] = models.OposChapter.objects.get(
                pk=self.kwargs['chapter_pk']
                ).course
        return context

    def forms_valid(self, form, inlines):
        chapter = shortcuts.get_object_or_404(models.OposChapter,
                                              id=self.kwargs['chapter_pk'])
        self.object.chapter = chapter
        self.object.save()
        response = super(StepCreate, self).forms_valid(form, inlines)
        return response

    def get_success_url(self):
        return reverse_lazy('opos:chapter',
                            kwargs={'course_slug': 'some',
                                    'course_pk': '1',
                                    'slug': 'slug',
                                    'pk': self.kwargs['chapter_pk']})

    def get_permission_object(self):
        return models.OposChapter.objects.get(
                pk=self.kwargs['chapter_pk']
                ).course


class StepUpdate(PermissionRequiredMixin, UpdateWithInlinesView):
    model = models.OposStep
    form_class = forms.StepForm
    inlines = [forms.FilesInline]
    template_name = 'add.html'
    success_message = "%(name)s was updated successfully"
    permission_required = 'lms_opos.change_oposstep'

    def get_context_data(self, **kwargs):
        context = super(StepUpdate, self).get_context_data(**kwargs)
        context['course'] = self.object.chapter.course
        return context

    def get_success_url(self):
        return reverse('opos:step',
                       kwargs={'course_slug': self.object.chapter.course.slug,
                               'course_pk': self.object.chapter.course.pk,
                               'chapter_slug': self.object.chapter.slug,
                               'chapter_pk': self.object.chapter.pk,
                               'slug': self.object.slug,
                               'pk': self.object.pk})

    def get_permission_object(self):
        return self.get_object().chapter.course


class StepDelete(PermissionRequiredMixin, DeleteView):
    model = models.OposStep
    form_class = forms.StepForm
    template_name = 'delete.html'
    success_message = "%(name)s was deleted successfully"
    permission_required = 'lms_opos.delete_oposstep'

    def get_context_data(self, **kwargs):
        context = super(StepDelete, self).get_context_data(**kwargs)
        context['course'] = self.object.chapter.course
        return context

    def get_success_url(self):
        return reverse('opos:chapter',
                       kwargs={'course_slug': self.object.chapter.course.slug,
                               'course_pk': self.object.chapter.course.pk,
                               'slug': self.object.chapter.slug,
                               'pk': self.object.chapter.pk})

    def get_permission_object(self):
        return self.get_object().chapter.course


class NotFoundView(TemplateView):
    template_name = "pages/404_opos.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context, status=404)


class GlossaryView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(GlossaryView, self).get_context_data(**kwargs)
        context['oposglossaryitem_list'] = OrderedDict()
        for letter in ascii_letters:
            context['oposglossaryitem_list'][letter] = models.OposGlossaryItem.objects.all().filter(title__startswith=letter)  # NOQA
        return OrderedDict(context)


class OposGlossaryItemUpdate(PermissionRequiredMixin, UpdateView):
    model = models.OposGlossaryItem
    success_url = reverse_lazy('opos:glossar')
    success_message = "%(name)s was updated successfully"
    form_class = forms.GlossaryItemForm
    template_name = 'add.html'
    permission_required = 'lms_opos.change_glossaryitemupdate'

    def get_permission_object(self):
        return models.OposGlossaryItem


class OposGlossaryItemCreate(PermissionRequiredMixin, CreateView):
    model = models.OposGlossaryItem
    success_message = "%(title)s was created successfully"
    form_class = forms.GlossaryItemForm
    template_name = 'add.html'
    permission_required = 'lms_opos.create_glossaryitemadd'

    def get_success_url(self):
        return reverse('opos:glossar')

    def get_permission_object(self):
        return models.OposGlossaryItem


class OposGlossaryItemDelete(PermissionRequiredMixin, DeleteView):
    model = models.OposGlossaryItem
    form_class = forms.GlossaryItemForm
    template_name = 'delete.html'
    success_message = "%(name)s was deleted successfully"
    permission_required = 'lms_opos.delete_oposglossaryitem'

    def get_success_url(self):
        return reverse('opos:glossar')

    def get_permission_object(self):
        return models.OposGlossaryItem


@login_required
def step_update_order(request):
    if request.POST:
        chapter_id = request.POST.get('pk')
        step_order = request.POST.getlist('order[]')
        chapter = models.OposChapter.objects.get(pk=chapter_id)
        # Get a list of steps
        steps = []
        for step_id in step_order:
            step = models.OposStep.objects.get(pk=step_id, chapter=chapter)
            steps.append(step)
        # find the max value of order
        max_order = max(step.order for step in steps) + 1
        # avoid possible colissions
        for step in steps:
            step.order = step.order + max_order
            step.save()
        # set the new step order
        for i, step in enumerate(steps):
            step.order = i
            step.save()
    return http.HttpResponse('o.k.')
