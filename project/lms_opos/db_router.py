# DB router for opos


class OposRouter(object):
    """
    A router to control all database operations on models in the
    lms_opos application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read lms_opos models go to db_opos.
        """
        if model._meta.app_label == 'lms_opos':
            return 'db_opos'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to lms_opos.
        """
        if model._meta.app_label == 'lms_opos':
            return 'db_opos'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the lms_opos app is involved.
        """
        if obj1._meta.app_label == 'db_opos' or \
           obj2._meta.app_label == 'db_opos':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the lms_opos app only appears in the 'db_opos'
        database.
        """
        if app_label == 'lms_opos':
            return db == 'db_opos'
        return None
