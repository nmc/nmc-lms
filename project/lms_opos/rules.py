# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import rules


# Predicates
@rules.predicate
def is_in_group(user, course):

    if not course:
        return False

    for owner in course.owner.all():

        if owner in user.groups.all():
            return True

    return False


# override default Permissions
rules.add_perm('lms_opos.add_oposstep', is_in_group)
rules.add_perm('lms_opos.change_oposstep', is_in_group)
rules.add_perm('lms_opos.delete_oposstep', is_in_group)
rules.add_perm('lms_opos.add_oposchapter', is_in_group)
rules.add_perm('lms_opos.change_oposchapter', is_in_group)
rules.add_perm('lms_opos.delete_oposchapter', is_in_group)
rules.add_perm('lms_opos.add_oposcourse', is_in_group)
rules.add_perm('lms_opos.change_oposcourse', is_in_group)
rules.add_perm('lms_opos.delete_oposcourse', is_in_group)
rules.add_perm('lms_opos.add_glossaryitem', is_in_group)
rules.add_perm('lms_opos.change_glossaryitem', is_in_group)
rules.add_perm('lms_opos.delete_glossaryitem', is_in_group)
