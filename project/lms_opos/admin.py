# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import (OposStep,
                     OposChapter,
                     OposCourse,
                     OposStepFile,
                     OposGlossaryItem)

from simple_history.admin import SimpleHistoryAdmin


class OposStepInline(admin.StackedInline):
    """Admin customizations."""
    model = OposStepFile


class OposStepAdmin(SimpleHistoryAdmin):
    """Admin customizations."""
    inlines = [OposStepInline, ]


class OposChapterAdmin(SimpleHistoryAdmin):
    """Admin customizations."""

    pass


class OposCourseAdmin(SimpleHistoryAdmin):
    """Admin customizations."""

    pass


class OposGlossaryItemAdmin(SimpleHistoryAdmin):

    pass


admin.site.register(OposStep, OposStepAdmin)
admin.site.register(OposChapter, OposChapterAdmin)
admin.site.register(OposCourse, OposCourseAdmin)
admin.site.register(OposGlossaryItem, OposGlossaryItemAdmin)
