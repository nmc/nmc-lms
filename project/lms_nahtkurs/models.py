# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.db import models
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible
from lms_base.models import StepMixin, ChapterMixin, CourseMixin


def course_directory_path(instance, filename):
    return '{0}/{1}'.format(instance.course.slug, filename)


class NahtkursStep(StepMixin):  # noqa 405
    chapter = models.ForeignKey(
            'NahtkursChapter', null=True, related_name='steps'
            )

    def get_course(self):
        return self.chapter.course

    def get_absolute_url(self):
        return reverse('nahtkurs:step', args=[str(self.chapter.course.slug),
                                              str(self.chapter.course.id),
                                              str(self.chapter.slug),
                                              str(self.chapter.id),
                                              str(self.slug),
                                              str(self.id), ])

    def get_step_number(self):
        chapter_list = list(self.chapter.course.chapters.all()
                            .order_by('order'))
        chapter_num = str(chapter_list.index(self.chapter) + 1)
        step_list = list(self.chapter.steps.all().order_by('order'))
        step_num = str(step_list.index(self) + 1)
        index = chapter_num + "." + step_num
        return index

    def save(self, *args, **kwargs):
        if not self.chapter.steps.all():
            self.order = 0
        if self.order is None:
            self.order = max(step.order for step in
                             self.chapter.steps.all()) + 1
        super(NahtkursStep, self).save(*args, **kwargs)


def step_directory_path(instance, filename):
    return '{0}/{1}/{2}/{3}'.format(
        instance.step.chapter.course.slug,
        instance.step.chapter.slug,
        instance.step.slug,
        filename)


@python_2_unicode_compatible
class NahtkursStepFile(models.Model):
    title = models.CharField(max_length=70)
    step = models.ForeignKey('NahtkursStep', related_name='files')
    file = models.FileField(upload_to=step_directory_path, max_length=255)

    def __str__(self):
        return self.title


class NahtkursChapter(ChapterMixin):  # noqa 405

    course = models.ForeignKey(
            'NahtkursCourse', null=True, related_name='chapters'
            )

    def get_course(self):
        return self.course

    def get_absolute_url(self):
        return reverse('nahtkurs:chapter', args=[str(self.course.slug),
                                                 str(self.course.id),
                                                 str(self.slug),
                                                 str(self.id), ])

    def save(self, *args, **kwargs):
        if not self.course.chapters.all():
            self.order = 0
        if self.order is None:
            self.order = max(chapter.order for chapter in
                             self.course.chapters.all()) + 1
        super(NahtkursChapter, self).save(*args, **kwargs)


class NahtkursCourse(CourseMixin):  # noqa 405

    def get_course(self):
        return self

    def get_absolute_url(self):
        return reverse('nahtkurs:course', args=[str(self.slug),
                                                str(self.id), ])


@python_2_unicode_compatible
class NahtkursAuthor(models.Model):

    name = models.TextField(max_length=None)
    image = models.ImageField(upload_to=course_directory_path,
                              max_length=255, blank=True, null=True)
    course = models.ForeignKey('NahtkursCourse', related_name='author',
                               verbose_name='author')

    def __str__(self):
        return self.name
