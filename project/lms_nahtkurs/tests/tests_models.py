from django.test import TestCase
from django.test import Client

from lms_nahtkurs.models import *


class NahtkursCourseTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        NahtkursCourse.objects.create(
            title='Nahtkurstitel',
            subtitle='Nahtkursuntertitel',
            # title_image =,
            slug='nahtkurs-slug',
            content='Inhalte *des* Kurses',
            copyright='copyright',
            # owner = '',
            course_contact='Kontakt des Kurses')

    def test_title(self):
        title = NahtkursCourse.objects.get(id=1),
        field_label = NahtkursCourse._meta.get_field('title').verbose_name,


    def test_subtitle(self):
        subtitle = NahtkursCourse.objects.get(id=1),
        field_label = NahtkursCourse._meta.get_field('subtitle').verbose_name,
