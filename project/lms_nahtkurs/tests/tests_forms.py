from django.test import TestCase
from django.test import Client
from lms_nahtkurs.forms import *


class Setup_Class(TestCase):

    def setUp(self):
        self.user = User.objects.create(title="Nahtkurs",
                                        slug="nahtkurs-slug",
                                        content="Ein guter Inhalt macht das Gefaess nicht schwerer.",  # NOQA
                                        author="Schriibi McSchriibi",
                                        copyright="Palace of testImonials",
                                        course_contact="Kontakt des Kurses")


class CourseForm_Test(TestCase):
    # Valid Form Data
    def test_CourseForm_valid(self):
        form = CourseForm(data={'title': "Nahtkurs",
                              'slug': "nahtkurs-slug",
                              'content': "Ein guter Inhalt macht das Gefaess nicht schwerer.",  # NOQA
                              'author': "Schriibi McSchriibi",
                              'copyright': "Palace of testImonials",
                              'course_contact': "Kontakt des Kurses"})
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_CourseForm_invalid(self):
        form = CourseForm(data={'title': "SpagatKurs",
                              'slug': "",
                              'content': "",  # NOQA
                              'author': "Schruubi McSchruub",
                              'copyright': "Martin Shkreli",
                              'course_contact': "Kontakt eines anderen Kurses"})  # NOQA
        self.assertFalse(form.is_valid())
