import unittest
import time
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


class SeleniumTests(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.set_headless(True)
        options.add_argument("--headless")

        profile = FirefoxProfile()
        profile.set_preference("intl.accept_languages", "de")

        self.driver = webdriver.Firefox(options=options, firefox_profile=profile)  # NOQA
        self.driver.set_window_size(1280, 1000)
        self.driver.implicitly_wait(10)
        self.wait = WebDriverWait(self.driver, 10)

    def tearDown(self):
        self.driver.quit()
        super(SeleniumTests, self).tearDown()

    def login(self):
        # Opening the link to login
        self.driver.get('http://0.0.0.0:8089/accounts/login')
        user = self.driver.find_element_by_id('id_login')
        password = self.driver.find_element_by_id('id_password')
        submit = self.driver.find_element_by_class_name('btn-primary')
        # Fill the form with data
        user.send_keys('tester')
        password.send_keys('tester')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

    def wait_for_load(self):
        time.sleep(2)

    def test_tales_title(self):
        self.driver.get('http://0.0.0.0:8089/')
        assert 'Tales' in self.driver.title

    def test_login(self):
        self.login()
        self.wait.until(lambda s: s.find_element(By.CLASS_NAME, "alert-success").is_displayed())  # NOQA
        # check the returned result
        assert 'Erfolgreich' in self.driver.find_element_by_class_name('alert-success').text  # NOQA
        # Check if Login / Logout Link in footer switches
        assert 'Abmelden' in self.driver.find_element_by_css_selector('footer div ul li:last-child a').text  # NOQA

    def test_logout(self):
        self.login()
        self.driver.find_element_by_link_text('Abmelden').click()
        self.wait_for_load()
        self.driver.find_element_by_class_name('btn-danger').click()
        self.wait.until(lambda s: s.find_element(By.CLASS_NAME, "alert-success").is_displayed())  # NOQA
        assert 'abgemeldet' in self.driver.find_element_by_class_name('alert-success').text  # NOQA
        # Check if Login / Logout Link in footer switches
        assert 'Internes Login' in self.driver.find_element_by_css_selector('footer div ul li:last-child a').text  # NOQA

    def test_info(self):
        self.driver.get('http://0.0.0.0:8089/')
        self.driver.find_element_by_link_text('Info').click()
        self.wait_for_load()
        self.wait.until(lambda s: s.find_element(By.ID, "about-modal").is_displayed())  # NOQA
        assert 'INFO' in self.driver.find_element_by_class_name('footnote-title').text  # NOQA

    def test_hilfe(self):
        self.driver.get('http://0.0.0.0:8089/')
        self.driver.find_element_by_link_text('Hilfe').click()
        self.wait.until(lambda s: s.find_element(By.ID, "help-modal").is_displayed())  # NOQA
        assert 'http://0.0.0.0:8089/static/images/help.jpg' in self.driver.find_element_by_css_selector('img.help-img').get_attribute('src')  # NOQA

    def test_impressum(self):
        self.driver.get('http://0.0.0.0:8089/')
        self.driver.find_element_by_link_text('Impressum').click()
        self.wait_for_load()
        self.wait.until(lambda s: s.find_element(By.ID, "impressum-modal").is_displayed())  # NOQA
        assert 'KONTAKTADRESSE' in self.driver.find_element_by_css_selector('div.modal-body h1:first-child').text  # NOQA

    def test_link_to_unibas(self):
        self.driver.get('http://0.0.0.0:8089/')
        self.driver.find_element_by_css_selector('.uni-logo a').click()
        self.wait_for_load()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertEqual(self.driver.current_url, 'https://www.unibas.ch/de')
