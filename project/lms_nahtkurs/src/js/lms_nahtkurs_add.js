/* global $, document, window */
/* eslint prefer-template: "error" */
/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* jshint esversion: 6 */

$('body').on('click', '[data-toggle="modal"]', function () {
    // eslint-disable-next-line
    $ ($ (this).data ("target")+' .modal-body').load ($ (this).attr ('href'));
});
