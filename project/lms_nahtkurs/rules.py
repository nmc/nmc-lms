# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import rules


# Predicates
@rules.predicate
def is_in_group(user, course):

    if not course:
        return False

    for owner in course.owner.all():

        if owner in user.groups.all():
            return True

    return False


# override default Permissions
rules.add_perm('lms_nahtkurs.add_nahtkursstep', is_in_group)
rules.add_perm('lms_nahtkurs.change_nahtkursstep', is_in_group)
rules.add_perm('lms_nahtkurs.delete_nahtkursstep', is_in_group)
rules.add_perm('lms_nahtkurs.add_nahtkurschapter', is_in_group)
rules.add_perm('lms_nahtkurs.change_nahtkurschapter', is_in_group)
rules.add_perm('lms_nahtkurs.delete_nahtkurschapter', is_in_group)
rules.add_perm('lms_nahtkurs.add_nahtkurscourse', is_in_group)
rules.add_perm('lms_nahtkurs.change_nahtkurscourse', is_in_group)
rules.add_perm('lms_nahtkurs.delete_nahtkurscourse', is_in_group)
rules.add_perm('lms_nahtkurs.add_glossaritem', is_in_group)
rules.add_perm('lms_nahtkurs.change_glossaritem', is_in_group)
rules.add_perm('lms_nahtkurs.delete_glossaritem', is_in_group)
