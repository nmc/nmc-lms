# DB router for nahtkurs


class NahtkursRouter(object):
    """
    A router to control all database operations on models in the
    lms_nahtkurs application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read lms_nahtkurs models go to db_nahtkurs.
        """
        if model._meta.app_label == 'lms_nahtkurs':
            return 'db_nahtkurs'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to lms_nahtkurs.
        """
        if model._meta.app_label == 'lms_nahtkurs':
            return 'db_nahtkurs'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the lms_nahtkurs app is involved.
        """
        if obj1._meta.app_label == 'db_nahtkurs' or \
           obj2._meta.app_label == 'db_nahtkurs':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the lms_nahtkurs app only appears in the 'db_nahtkurs'
        database.
        """
        if app_label == 'lms_nahtkurs':
            return db == 'db_nahtkurs'
        return None
