# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import (NahtkursStep,
                     NahtkursChapter,
                     NahtkursCourse,
                     NahtkursStepFile,
                     NahtkursAuthor)

from simple_history.admin import SimpleHistoryAdmin


class NahtkursStepInline(admin.StackedInline):
    """Admin customizations."""
    model = NahtkursStepFile


class NahtkursStepAdmin(SimpleHistoryAdmin):
    """Admin customizations."""
    inlines = [NahtkursStepInline, ]


class NahtkursChapterAdmin(SimpleHistoryAdmin):
    """Admin customizations."""

    pass


class NahtkursAuthorInline(admin.StackedInline):
    """Admin customizations."""
    model = NahtkursAuthor
    extra = 0


class NahtkursCourseAdmin(SimpleHistoryAdmin):
    """Admin customizations."""
    inlines = [NahtkursAuthorInline, ]


admin.site.register(NahtkursStep, NahtkursStepAdmin)
admin.site.register(NahtkursChapter, NahtkursChapterAdmin)
admin.site.register(NahtkursCourse, NahtkursCourseAdmin)
