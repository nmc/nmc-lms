# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.forms import ModelForm

from extra_views import InlineFormSet

from lms_nahtkurs import widgets, models


class CourseForm(ModelForm):
    class Meta:
        model = models.NahtkursCourse
        exclude = ['slug', 'owner', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'author': widgets.CustomisableMarkdownxWidget(),
            'copyright': widgets.CustomisableMarkdownxWidget(),
            'course_contact': widgets.CustomisableMarkdownxWidget(),
        }


class AuthorInline(InlineFormSet):
    model = models.NahtkursAuthor
    fields = '__all__'
    prefix = 'authors'
    extra = 1


class UpdateCourseForm(CourseForm):
    title_image = forms.ImageField(required=False)


class ChapterForm(ModelForm):
    class Meta:
        model = models.NahtkursChapter
        exclude = ['slug', 'order', 'course', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
        }


class StepForm(ModelForm):
    class Meta:
        model = models.NahtkursStep
        exclude = ['slug', 'order', 'chapter', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'copyright': widgets.CustomisableMarkdownxWidget(),
        }


class FilesInline(InlineFormSet):
    model = models.NahtkursStepFile
    fields = ['title', 'file']
    prefix = 'files'
    extra = 1
