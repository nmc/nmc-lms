# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import url
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

from lms_nahtkurs import views

static_urls = [
    # markdown tutorial
    url(
        r'^markdown$',
        TemplateView.as_view(template_name='markdown.html'),
        name='markdown'
    ),
    url(
        r'^impressum$',
        TemplateView.as_view(template_name='impressum.html',),
        name='impressum'
    ),
    url(
        r'^about$',
        TemplateView.as_view(template_name='pages/nahtkurs_about.html',),
        name='about'
    ),
    url(
        r'^help$',
        TemplateView.as_view(template_name='help.html',),
        name='help'
    ),
    url(
        r'^glossar$',
        TemplateView.as_view(template_name='pages/nahtkurs_glossar.html',),
        name='glossar'
    ),
]

course_urls = [
    # course list
    url(r'^$',
        RedirectView.as_view(url=reverse_lazy('nahtkurs:course',
                             kwargs={'slug': 'nahtkurs', 'pk': '2'}
                                            )
                             ),
        name='redirect_nahtkurs'),
    url(
        r'^course/add$',
        views.CourseCreate.as_view(),
        name='course_add'
    ),
    url(
        r'^course/edit/(?P<pk>\d+)$',
        never_cache(views.CourseUpdate.as_view()),
        name='course_edit'
    ),
    url(
        r'^course/delete/(?P<pk>\d+)$',
        views.CourseDelete.as_view(),
        name='course_delete'
    ),
    # course overview / chapter list
    url(
        r'^(?P<slug>[\w-]+)-(?P<pk>\d+)/$',
        views.CourseDetails.as_view(
            template_name='nahtkurs_course_home.html',
            ),
        name='course'
    ),
    url(
        r'^(?P<slug>[\w-]+)-(?P<pk>\d+)/kontakt$',
        views.CourseDetails.as_view(
            template_name='nahtkurs_kontakt.html',
            ),
        name='kontakt'
    ),
]

chapter_urls = [
    url(
        r'^chapter/add/(?P<course_pk>\d+)$',
        views.ChapterCreate.as_view(),
        name='chapter_add'
    ),
    url(
        r'^chapter/edit/(?P<pk>\d+)$',
        never_cache(views.ChapterUpdate.as_view()),
        name='chapter_edit'
    ),
    url(
        r'^chapter/delete/(?P<pk>\d+)$',
        views.ChapterDelete.as_view(),
        name='chapter_delete'
    ),
    # chapter overview / step list
    url(
        r'^(?P<course_slug>[\w-]+)-(?P<course_pk>\d+)/'
        r'(?P<slug>[\w-]+)-(?P<pk>\d+)/$',
        views.ChapterDetails.as_view(
            template_name='nahtkurs_chapter_home.html',
            ),
        name='chapter'
    ),
    url(
        r'^chapter_order$',
        views.chapter_update_order,
        name='chapter_update_order'
    ),
]

step_urls = [
    url(
        r'^step/add/(?P<chapter_pk>\d+)$',
        views.StepCreate.as_view(),
        name='step_add'
    ),
    url(
        r'^step/edit/(?P<pk>\d+)$',
        never_cache(views.StepUpdate.as_view()),
        name='step_edit'
    ),
    url(
        r'^step/delete/(?P<pk>\d+)$',
        views.StepDelete.as_view(),
        name='step_delete'
    ),
    # step
    url(
        r'^(?P<course_slug>[\w-]+)-(?P<course_pk>\d+)/'
        r'(?P<chapter_slug>[\w-]+)-(?P<chapter_pk>\d+)/'
        r'(?P<slug>[\w-]+)-(?P<pk>\d+)$',
        views.StepDetails.as_view(
            template_name='nahtkurs_step_home.html',
            ),
        name='step'
    ),
    url(
        r'^step_order$',
        views.step_update_order,
        name='step_update_order',
    ),
]


urlpatterns = static_urls + course_urls + chapter_urls + step_urls

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^404/$', views.NotFoundView.as_view()),
    ]
