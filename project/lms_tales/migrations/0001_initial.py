# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-11-27 11:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import lms_base.models
import lms_tales.models
import markdownx.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lms_base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalTalesChapter',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('order', models.IntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical tales chapter',
            },
        ),
        migrations.CreateModel(
            name='HistoricalTalesCourse',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('subtitle', models.CharField(blank=True, max_length=70)),
                ('subtitle_de', models.CharField(blank=True, max_length=70, null=True)),
                ('subtitle_en', models.CharField(blank=True, max_length=70, null=True)),
                ('title_image', models.TextField(default='default.jpg', max_length=255)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('content', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('content_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('content_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('author', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('author_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('author_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('copyright_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('course_contact', markdownx.models.MarkdownxField(blank=True, help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical tales course',
            },
        ),
        migrations.CreateModel(
            name='HistoricalTalesStep',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('subtitle', models.CharField(blank=True, max_length=70)),
                ('subtitle_de', models.CharField(blank=True, max_length=70, null=True)),
                ('subtitle_en', models.CharField(blank=True, max_length=70, null=True)),
                ('title_image', models.TextField(blank=True, max_length=255)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('content', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('content_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('content_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('copyright_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('step_type', models.CharField(choices=[('TA', 'Audio'), ('TV', 'Video'), ('TT', 'Text'), ('TM', 'Mixed'), ('TQ', 'Quiz')], default='TT', max_length=2)),
                ('duration', models.CharField(blank=True, help_text="Die Dauer kann in Minuten angegeben werden. Beispiel: '10 Minuten'", max_length=70)),
                ('order', models.IntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical tales step',
            },
        ),
        migrations.CreateModel(
            name='TalesChapter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('order', models.IntegerField()),
            ],
            options={
                'ordering': ['course', 'order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TalesCourse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('subtitle', models.CharField(blank=True, max_length=70)),
                ('subtitle_de', models.CharField(blank=True, max_length=70, null=True)),
                ('subtitle_en', models.CharField(blank=True, max_length=70, null=True)),
                ('title_image', models.ImageField(default='default.jpg', max_length=255, upload_to=lms_base.models.course_directory_path)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('content', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('content_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('content_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('author', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('author_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('author_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('copyright_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('course_contact', markdownx.models.MarkdownxField(blank=True, help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('owner', models.ManyToManyField(to='auth.Group')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TalesGlossaryItem',
            fields=[
                ('glossaryitemmixin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='lms_base.GlossaryItemMixin')),
            ],
            bases=('lms_base.glossaryitemmixin',),
        ),
        migrations.CreateModel(
            name='TalesStep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('title_de', models.CharField(max_length=70, null=True)),
                ('title_en', models.CharField(max_length=70, null=True)),
                ('subtitle', models.CharField(blank=True, max_length=70)),
                ('subtitle_de', models.CharField(blank=True, max_length=70, null=True)),
                ('subtitle_en', models.CharField(blank=True, max_length=70, null=True)),
                ('title_image', models.ImageField(blank=True, max_length=255, upload_to=lms_base.models.course_directory_path)),
                ('slug', models.CharField(editable=False, max_length=200)),
                ('content', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('content_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('content_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.')),
                ('copyright_de', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('copyright_en', markdownx.models.MarkdownxField(help_text='Dieses Feld kannst Du mit Markdown formatieren.', null=True)),
                ('step_type', models.CharField(choices=[('TA', 'Audio'), ('TV', 'Video'), ('TT', 'Text'), ('TM', 'Mixed'), ('TQ', 'Quiz')], default='TT', max_length=2)),
                ('duration', models.CharField(blank=True, help_text="Die Dauer kann in Minuten angegeben werden. Beispiel: '10 Minuten'", max_length=70)),
                ('order', models.IntegerField()),
                ('chapter', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='steps', to='lms_tales.TalesChapter')),
            ],
            options={
                'ordering': ['chapter', 'order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TalesStepFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=70)),
                ('file', models.FileField(max_length=255, upload_to=lms_tales.models.step_directory_path)),
                ('step', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='lms_tales.TalesStep')),
            ],
        ),
        migrations.AddField(
            model_name='taleschapter',
            name='course',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='chapters', to='lms_tales.TalesCourse'),
        ),
        migrations.AddField(
            model_name='historicaltalesstep',
            name='chapter',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='lms_tales.TalesChapter'),
        ),
        migrations.AddField(
            model_name='historicaltalesstep',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicaltaleschapter',
            name='course',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='lms_tales.TalesCourse'),
        ),
        migrations.AddField(
            model_name='historicaltaleschapter',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='talesstep',
            unique_together=set([('order', 'chapter')]),
        ),
        migrations.AlterUniqueTogether(
            name='taleschapter',
            unique_together=set([('order', 'course')]),
        ),
    ]
