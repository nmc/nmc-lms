# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from modeltranslation.translator import translator, TranslationOptions

import simple_history

from .models import (TalesStep, TalesChapter, TalesCourse)


class TalesStepTranslationOptions(TranslationOptions):
    """Add translation options to talesstep."""

    fields = ('title', 'subtitle', 'content', 'copyright',)


class TalesChapterTranslationOptions(TranslationOptions):
    """Add translation options to taleschapter."""

    fields = ('title',)


class TalesCourseTranslationOptions(TranslationOptions):
    """Add translation options to talescourse."""

    fields = ('title', 'subtitle', 'content', 'copyright',)


translator.register(TalesStep, TalesStepTranslationOptions)
translator.register(TalesChapter, TalesChapterTranslationOptions)
translator.register(TalesCourse, TalesCourseTranslationOptions)

simple_history.register(TalesStep)
simple_history.register(TalesChapter)
simple_history.register(TalesCourse)
