# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import rules


# Predicates
@rules.predicate
def is_in_group(user, course):

    if not course:
        return False

    for owner in course.owner.all():

        if owner in user.groups.all():
            return True

    return False


# override default Permissions
rules.add_perm('lms_tales.add_talesstep', is_in_group)
rules.add_perm('lms_tales.change_talesstep', is_in_group)
rules.add_perm('lms_tales.delete_talesstep', is_in_group)
rules.add_perm('lms_tales.add_taleschapter', is_in_group)
rules.add_perm('lms_tales.change_taleschapter', is_in_group)
rules.add_perm('lms_tales.delete_taleschapter', is_in_group)
rules.add_perm('lms_tales.add_talescourse', is_in_group)
rules.add_perm('lms_tales.change_talescourse', is_in_group)
rules.add_perm('lms_tales.delete_talescourse', is_in_group)
rules.add_perm('lms_tales.add_glossaryitem', is_in_group)
rules.add_perm('lms_tales.change_glossaryitem', is_in_group)
rules.add_perm('lms_tales.delete_glossaryitem', is_in_group)
