# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.forms import ModelForm

from extra_views import InlineFormSet

from lms_tales import widgets, models


class CourseForm(ModelForm):
    class Meta:
        model = models.TalesCourse
        exclude = ['title', 'subtitle', 'content', 'copyright',
                   'slug', 'owner', ]
        widgets = {
            'content_de': widgets.CustomisableMarkdownxWidget(),
            'content_en': widgets.CustomisableMarkdownxWidget(),
            'author_de': widgets.CustomisableMarkdownxWidget(),
            'author_en': widgets.CustomisableMarkdownxWidget(),
            'copyright_de': widgets.CustomisableMarkdownxWidget(),
            'copyright_en': widgets.CustomisableMarkdownxWidget(),
            'course_contact': widgets.CustomisableMarkdownxWidget(),
        }


class AuthorInline(InlineFormSet):
    model = models.TalesAuthor
    fields = '__all__'
    prefix = 'author'
    extra = 1


class UpdateCourseForm(CourseForm):
    title_image = forms.ImageField(required=False)


class ChapterForm(ModelForm):
    class Meta:
        model = models.TalesChapter
        exclude = ['title', 'slug', 'order', 'course', ]
        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
        }


class StepForm(ModelForm):
    class Meta:
        model = models.TalesStep
        exclude = ['title', 'subtitle', 'content', 'copyright', 'slug',
                   'order', 'chapter', ]
        widgets = {
            'content_de': widgets.CustomisableMarkdownxWidget(),
            'content_en': widgets.CustomisableMarkdownxWidget(),
            'copyright_de': widgets.CustomisableMarkdownxWidget(),
            'copyright_en': widgets.CustomisableMarkdownxWidget(),
        }


class FilesInline(InlineFormSet):
    model = models.TalesStepFile
    fields = '__all__'
    prefix = 'files'
    extra = 1


class GlossaryItemForm(ModelForm):
    class Meta:
        model = models.TalesGlossaryItem
        exclude = []
        # fields = ['title', 'content', 'links', 'images', 'references']

        widgets = {
            'content': widgets.CustomisableMarkdownxWidget(),
            'references': widgets.CustomisableMarkdownxWidget(),
            'links': forms.SelectMultiple(),
        }
