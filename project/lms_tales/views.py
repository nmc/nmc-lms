# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.edit import (CreateView, DeleteView, UpdateView)
from django.core.urlresolvers import reverse_lazy, reverse
from django import shortcuts
from django import http
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from rules.contrib.views import PermissionRequiredMixin

from collections import OrderedDict

from extra_views import CreateWithInlinesView, UpdateWithInlinesView

from lms_tales import models, forms


ascii_letters = "#/=\"\'@$-_<>*(!§°0123456789aàäAÄbBcCdDeéèEfFgGhHiIjJkKlLmMnNoöOÖpPqQrRsStTuüUÜvVwWxXyYzZ"  # NOQA

home_page = None


# Course views
class CourseDetails(DetailView):
    model = models.TalesCourse

    def get(self, request, *args, **kwargs):
        response = super(CourseDetails, self).get(request, *args, **kwargs)
        if (self.object.slug != self.kwargs['slug']):
            return shortcuts.redirect(self.object, permanent=True)
        return response

    def get_context_data(self, **kwargs):
        context = super(CourseDetails, self).get_context_data(**kwargs)
        context['course_contact'] = self.object.course_contact
        return context


class CourseList(ListView):
    model = models.TalesCourse

    def get_context_data(self, **kwargs):
        context = super(CourseList, self).get_context_data(**kwargs)
        context['talesglossaryitem_list'] = {}
        context['hasGlossary'] = False
        for letter in ascii_letters:
            context['talesglossaryitem_list'][letter] = models.TalesGlossaryItem.objects.all().filter(title__startswith=letter)  # NOQA
        if (len(context['talesglossaryitem_list']) > 0):
                context['hasGlossary'] = True

        return context


class CourseCreate(PermissionRequiredMixin, CreateView):
    model = models.TalesCourse
    success_url = reverse_lazy('tales:course_list')
    success_message = "%(name)s was created successfully"
    form_class = forms.CourseForm
    template_name = 'add.html'
    permission_required = 'lms_tales.add_talescourse'


class CourseUpdate(PermissionRequiredMixin, UpdateWithInlinesView):
    model = models.TalesCourse
    success_url = reverse_lazy('tales:course_list')
    success_message = "%(name)s was updated successfully"
    form_class = forms.UpdateCourseForm
    inlines = [forms.AuthorInline]
    template_name = 'add.html'
    permission_required = 'lms_tales.change_talescourse'


class CourseDelete(PermissionRequiredMixin, DeleteView):
    model = models.TalesCourse
    success_url = reverse_lazy('tales:course_list')
    success_message = "%(name)s was deleted successfully"
    form_class = forms.CourseForm
    template_name = 'delete.html'
    permission_required = 'lms_tales.delete_talescourse'


# Chapter views
class ChapterDetails(DetailView):
    model = models.TalesChapter

    def get(self, request, *args, **kwargs):
        response = super(ChapterDetails, self).get(request, *args, **kwargs)
        if (
            self.object.slug != self.kwargs['slug'] or
            self.object.course.slug != self.kwargs['course_slug'] or
            self.object.course.pk != int(self.kwargs['course_pk'])
        ):
            return shortcuts.redirect(self.object, permanent=True)
        return response


class ChapterList(ListView):
    model = models.TalesChapter


class ChapterCreate(PermissionRequiredMixin, CreateView):
    model = models.TalesChapter
    form_class = forms.ChapterForm
    template_name = 'add.html'
    success_message = "%(name)s was created successfully"
    permission_required = 'lms_tales.add_taleschapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterCreate, self).get_context_data(**kwargs)
        context['course'] = models.TalesCourse.objects.get(
                pk=self.kwargs['course_pk']
                )
        return context

    def form_valid(self, form):
        course = shortcuts.get_object_or_404(models.TalesCourse,
                                             id=self.kwargs['course_pk'])
        form.instance.course = course
        form.instance.save()
        response = super(ChapterCreate, self).form_valid(form)
        return response

    def get_success_url(self):
        return reverse('tales:course',
                       kwargs={'slug': 'some',
                               'pk': self.kwargs['course_pk']})

    def get_permission_object(self):
        return models.TalesCourse.objects.get(pk=self.kwargs['course_pk'])


class ChapterUpdate(PermissionRequiredMixin, UpdateView):
    model = models.TalesChapter
    form_class = forms.ChapterForm
    template_name = 'add.html'
    success_message = "%(name)s was updated successfully"
    permission_required = 'lms_tales.change_taleschapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterUpdate, self).get_context_data(**kwargs)
        context['course'] = self.object.course
        return context

    def get_success_url(self):
        return reverse('tales:course',
                       kwargs={'slug': self.object.course.slug,
                               'pk': self.object.course.pk})

    def get_permission_object(self):
        return self.get_object().course


class ChapterDelete(PermissionRequiredMixin, DeleteView):
    model = models.TalesChapter
    form_class = forms.ChapterForm
    template_name = 'delete.html'
    success_message = "%(name)s was deleted successfully"
    permission_required = 'lms_tales.delete_taleschapter'

    def get_context_data(self, **kwargs):
        context = super(ChapterDelete, self).get_context_data(**kwargs)
        context['course'] = self.object.course
        return context

    def get_success_url(self):
        return reverse('tales:course',
                       kwargs={'slug': self.object.course.slug,
                               'pk': self.object.course.pk})

    def get_permission_object(self):
        return self.get_object().course


def chapter_update_order(request):
    if request.POST:
        course_id = request.POST.get('pk')
        chapter_order = request.POST.getlist('order[]')
        course = models.TalesCourse.objects.get(pk=course_id)
        # Get a list of chapters
        chapters = []
        for step_id in chapter_order:
            chapter = models.TalesChapter.objects.get(pk=step_id,
                                                      course=course)
            chapters.append(chapter)
        # find the max value of order
        max_order = max(chapter.order for chapter in chapters) + 1
        # avoid possible colissions
        for chapter in chapters:
            chapter.order = chapter.order + max_order
            chapter.save()
        # set the new chapter order
        for i, chapter in enumerate(chapters):
            chapter.order = i
            chapter.save()
    return http.HttpResponse('o.k.')


# Step views
class StepDetails(DetailView):
    model = models.TalesStep

    def get(self, request, *args, **kwargs):
        response = super(StepDetails, self).get(request, *args, **kwargs)
        if (
            self.object.slug != self.kwargs['slug'] or
            self.object.chapter.slug != self.kwargs['chapter_slug'] or
            self.object.chapter.pk != int(self.kwargs['chapter_pk']) or
            self.object.chapter.course.slug != self.kwargs['course_slug'] or
            self.object.chapter.course.pk != int(self.kwargs['course_pk'])
        ):
            return shortcuts.redirect(self.object, permanent=True)
        return response

    def get_context_data(self, **kwargs):
        context = super(StepDetails, self).get_context_data(**kwargs)
        context['course_contact'] = models.TalesChapter.objects.get(
                pk=self.kwargs['chapter_pk']
                ).course.course_contact
        return context


class StepList(ListView):
    model = models.TalesStep


class StepCreate(PermissionRequiredMixin, CreateWithInlinesView):
    model = models.TalesStep
    form_class = forms.StepForm
    inlines = [forms.FilesInline]
    template_name = 'add.html'
    success_message = "%(title)s was created successfully"
    permission_required = 'lms_tales.add_talesstep'

    def get_context_data(self, **kwargs):
        context = super(StepCreate, self).get_context_data(**kwargs)
        context['course'] = models.TalesChapter.objects.get(
                pk=self.kwargs['chapter_pk']
                ).course
        return context

    def forms_valid(self, form, inlines):
        chapter = shortcuts.get_object_or_404(models.TalesChapter,
                                              id=self.kwargs['chapter_pk'])
        self.object.chapter = chapter
        self.object.save()
        response = super(StepCreate, self).forms_valid(form, inlines)
        return response

    def get_success_url(self):
        return reverse_lazy('tales:chapter',
                            kwargs={'course_slug': 'some',
                                    'course_pk': '1',
                                    'slug': 'slug',
                                    'pk': self.kwargs['chapter_pk']})

    def get_permission_object(self):
        return models.TalesChapter.objects.get(
                pk=self.kwargs['chapter_pk']
                ).course


class StepUpdate(PermissionRequiredMixin, UpdateWithInlinesView):
    model = models.TalesStep
    form_class = forms.StepForm
    inlines = [forms.FilesInline]
    template_name = 'add.html'
    success_message = "%(name)s was updated successfully"
    permission_required = 'lms_tales.change_talesstep'

    def get_context_data(self, **kwargs):
        context = super(StepUpdate, self).get_context_data(**kwargs)
        context['course'] = self.object.chapter.course
        return context

    def get_success_url(self):
        return reverse('tales:step',
                       kwargs={'course_slug': self.object.chapter.course.slug,
                               'course_pk': self.object.chapter.course.pk,
                               'chapter_slug': self.object.chapter.slug,
                               'chapter_pk': self.object.chapter.pk,
                               'slug': self.object.slug,
                               'pk': self.object.pk})

    def get_permission_object(self):
        return self.get_object().chapter.course


class StepDelete(PermissionRequiredMixin, DeleteView):
    model = models.TalesStep
    form_class = forms.StepForm
    template_name = 'delete.html'
    success_message = "%(name)s was deleted successfully"
    permission_required = 'lms_tales.delete_talesstep'

    def get_context_data(self, **kwargs):
        context = super(StepDelete, self).get_context_data(**kwargs)
        context['course'] = self.object.chapter.course
        return context

    def get_success_url(self):
        return reverse('tales:chapter',
                       kwargs={'course_slug': self.object.chapter.course.slug,
                               'course_pk': self.object.chapter.course.pk,
                               'slug': self.object.chapter.slug,
                               'pk': self.object.chapter.pk})

    def get_permission_object(self):
        return self.get_object().chapter.course


def home(request):
    return render_to_response('home.html',
                              RequestContext(request))


class GlossaryView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(GlossaryView, self).get_context_data(**kwargs)
        context['talesglossaryitem_list'] = OrderedDict()
        for letter in ascii_letters:
            context['talesglossaryitem_list'][letter] = models.TalesGlossaryItem.objects.all().filter(title__startswith=letter)  # NOQA
        return OrderedDict(context)


class TalesGlossaryItemUpdate(PermissionRequiredMixin, UpdateView):
    model = models.TalesGlossaryItem
    success_url = reverse_lazy('tales:course_list')
    success_message = "%(name)s was updated successfully"
    form_class = forms.GlossaryItemForm
    template_name = 'add.html'
    permission_required = 'lms_tales.change_glossaryitemupdate'

    def get_permission_object(self):
        return models.TalesGlossaryItem


class TalesGlossaryItemCreate(PermissionRequiredMixin, CreateView):
    model = models.TalesGlossaryItem
    success_message = "%(title)s was created successfully"
    form_class = forms.GlossaryItemForm
    template_name = 'add.html'
    permission_required = 'lms_tales.create_glossaryitemadd'

    def get_success_url(self):
        return reverse('tales:course_list')

    def get_permission_object(self):
        return models.TalesGlossaryItem


class NotFoundView(TemplateView):
    template_name = "pages/404_tales.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context, status=404)


@login_required
def step_update_order(request):
    if request.POST:
        chapter_id = request.POST.get('pk')
        step_order = request.POST.getlist('order[]')
        chapter = models.TalesChapter.objects.get(pk=chapter_id)
        # Get a list of steps
        steps = []
        for step_id in step_order:
            step = models.TalesStep.objects.get(pk=step_id, chapter=chapter)
            steps.append(step)
        # find the max value of order
        max_order = max(step.order for step in steps) + 1
        # avoid possible colissions
        for step in steps:
            step.order = step.order + max_order
            step.save()
        # set the new step order
        for i, step in enumerate(steps):
            step.order = i
            step.save()
    return http.HttpResponse('o.k.')
