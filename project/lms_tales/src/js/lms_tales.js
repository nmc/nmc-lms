/* global $, document, window */
/* eslint prefer-template: "error" */
/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* jshint esversion: 6 */

$(document).ready(function () {
  //  cache DOM elements
  //  const mainContent = $('#main-content');
  const header = $('#header');
  const sidebar = $('#sidebar-nav');
  const toggler = $('#sidebar-toggle');
  const sidebarHeight = sidebar.outerHeight();
  const contentHeight = $('#titlebar').outerHeight() + $('#main-content').outerHeight() + $('#footer').outerHeight();
  let scrolling = false;

  function checkScrollbarPosition() {
    const scrollTop = $(window).scrollTop();
    const headerHeight = header.outerHeight();
    if (headerHeight - scrollTop <= 0 && sidebarHeight < contentHeight) {
      sidebar.addClass('fixed-top-sidebar');
      toggler.addClass('fixed-top-sidebar');
    } else {
      sidebar.removeClass('fixed-top-sidebar');
      toggler.removeClass('fixed-top-sidebar');
    }
    scrolling = false;
  }


  checkScrollbarPosition();
  $(window).on('scroll', function () {
    if (!scrolling) {
      // eslint-disable-next-line
      (!window.requestAnimationFrame) ? setTimeout(checkScrollbarPosition, 300) :
      window.requestAnimationFrame(checkScrollbarPosition);
      scrolling = true;
    }
  });
});
