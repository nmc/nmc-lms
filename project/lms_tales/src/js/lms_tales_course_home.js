/* global $, document, window */
/* eslint prefer-template: "error" */
/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* jshint esversion: 6 */

$(document).ready(function () {
  console.log('in..')
  const list = document.getElementById('chapter-list');
  const listSmall = document.getElementById('chapter-list-small');

  // eslint-disable-next-line
  const csrftoken = Cookies.get('csrftoken');

  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  $.ajaxSetup({
    // eslint-disable-next-line
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader('X-CSRFToken', csrftoken);
      }
    },
  });

  // eslint-disable-next-line
  const sortable = Sortable.create(list, {
    handle: '.handle',
    // eslint-disable-next-line
    onSort: function(evt) {
      const order = sortable.toArray();
      const course = {
        // eslint-disable-next-line
        pk: object_pk,
        // eslint-disable-next-line
        order: order,
      };
      $.ajax({
        type: 'POST',
        // eslint-disable-next-line
        url: urlpath,
        // eslint-disable-next-line
        data: course,
        // eslint-disable-next-line
        success: function (data) {
          // eslint-disable-next-line
          console.log ('success');
        },
        // eslint-disable-next-line
        error: function(msg) {
          // eslint-disable-next-line
          alert('Theres an error with the server.');
        },
      });
    },
  });

  // eslint-disable-next-line
  const sortableSmall = Sortable.create(listSmall, {
    handle: '.handle',
    // eslint-disable-next-line
    onSort: function(evt) {
      const order = sortableSmall.toArray();
      const course = {
        // eslint-disable-next-line
        pk: object_pk,
        // eslint-disable-next-line
        order: order,
      };
      $.ajax({
        type: 'POST',
        // eslint-disable-next-line
        url: urlpath,
        // eslint-disable-next-line
        data: course,
        // eslint-disable-next-line
        success: function (data) {
          // eslint-disable-next-line
          console.log ('success');
        },
        // eslint-disable-next-line
        error: function(msg) {
          // eslint-disable-next-line
          alert('Theres an error with the server.');
        },
      });
    },
  });
});
