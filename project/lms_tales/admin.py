# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import (TalesStep,
                     TalesChapter,
                     TalesCourse,
                     TalesStepFile,
                     TalesGlossaryItem,
                     TalesAuthor)

from simple_history.admin import SimpleHistoryAdmin


class TalesStepInline(admin.StackedInline):
    """Admin customizations."""
    model = TalesStepFile


class TalesStepAdmin(SimpleHistoryAdmin):
    """Admin customizations."""
    inlines = [TalesStepInline, ]


class TalesChapterAdmin(SimpleHistoryAdmin):
    """Admin customizations."""

    pass


class TalesAuthorInline(admin.StackedInline):
    """Admin customizations."""
    model = TalesAuthor
    extra = 0


class TalesCourseAdmin(SimpleHistoryAdmin):
    """Admin customizations."""
    inlines = [TalesAuthorInline, ]


class TalesGlossaryItemAdmin(SimpleHistoryAdmin):

    pass


admin.site.register(TalesStep, TalesStepAdmin)
admin.site.register(TalesChapter, TalesChapterAdmin)
admin.site.register(TalesCourse, TalesCourseAdmin)
admin.site.register(TalesGlossaryItem, TalesGlossaryItemAdmin)
