# DB router for tales


class TalesRouter(object):
    """
    A router to control all database operations on models in the
    lms_tales application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read lms_tales models go to db_tales.
        """
        if model._meta.app_label == 'lms_tales':
            return 'db_tales'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write tales models go to lms_tales.
        """
        if model._meta.app_label == 'lms_tales':
            return 'db_tales'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the lms_tales app is involved.
        """
        if obj1._meta.app_label == 'db_tales' or \
           obj2._meta.app_label == 'db_tales':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the lms_tales app only appears in the 'db_tales'
        database.
        """
        if app_label == 'lms_tales':
            return db == 'db_tales'
        return None
