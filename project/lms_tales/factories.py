# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import random

import factory
from factory.django import DjangoModelFactory

from django.contrib.auth import models
from django.contrib.auth.hashers import make_password

from users.models import User

from lms_tales.models import TalesStep, TalesCourse, TalesChapter


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = models.Group
        django_get_or_create = ('name',)

    name = factory.Sequence(lambda n: "Group #%s" % n)


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username',)

    username = factory.Faker('first_name')
    password = make_password('nmc')

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for group in extracted:
                self.groups.add(group)


class AdminFactory(UserFactory):
    username = 'admin'
    password = make_password('admin')
    is_superuser = True
    is_staff = True


class CourseFactory(DjangoModelFactory):
    class Meta:
        model = TalesCourse
        django_get_or_create = ('title',)

    title = factory.Faker('bs')
    content = factory.Faker('text')
    copyright = factory.Faker('name')
    # owner = factory.SubFactory(GroupFactory)


# class AuthorFactory(DjangoModelFactory):
#     class Meta:
#         model = TalesChapter
#         django_get_or_create = ('name')
#
#     name = factory.Faker('name')
#     image = factory.Faker('text')
#     course = factory.Faker('name')


class ChapterFactory(DjangoModelFactory):
    class Meta:
        model = TalesChapter
        django_get_or_create = ('course', 'order')

    title = factory.Faker('bs')
    order = factory.Sequence(lambda n: n)
    course = factory.Iterator(TalesCourse.objects.all())


class StepFactory(DjangoModelFactory):
    class Meta:
        model = TalesStep
        django_get_or_create = ('order', 'chapter')

    title = factory.Faker('bs')
    content = factory.Faker('text')
    copyright = factory.Faker('name')
    step_type = factory.Iterator([TalesStep.TYPE_AUDIO,
                                  TalesStep.TYPE_TEXT,
                                  TalesStep.TYPE_VIDEO])
    duration = "{0:d} min".format(random.randint(10, 30))
    order = factory.Sequence(lambda n: n)
    chapter = factory.Iterator(TalesChapter.objects.all())
