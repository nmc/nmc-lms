# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import random

from django.core.management.base import BaseCommand

from lms_tales import factories


class Command(BaseCommand):
    def handle(self, *args, **options):
        groups = factories.GroupFactory.create_batch(size=5)
        users = factories.UserFactory.create_batch(size=20)
        for user in users:
            # add each user to up to 3 random groups
            for n in range(1, 3):
                user.groups.add(groups[random.randint(0, len(groups)-1)])

        factories.AdminFactory.create()
        factories.CourseFactory.create_batch(size=5)
        factories.ChapterFactory.create_batch(size=30)
        factories.StepFactory.create_batch(size=100)
