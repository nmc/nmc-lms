/* global $, document, window */
/* eslint prefer-template: "error" */
/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* jshint esversion: 6 */

// eslint-disable-next-line
function checkNote(button, note) {
  if (note === 'a') {
    $('#result').html('Der Ton in diesem Beispiel, der nicht in der Teiltonreihe (von C aus) vorkommt ist das a‘. Die Melodie kann in dieser Form also nicht auf dem Alphorn gespielt werden.');
    $('#result').removeClass();
    $('#result').addClass('answer_not_correct');
    $(button).addClass('button_not_correct');
  } else {
    $('#result').html(`Ton ${note}' kann auf dem Alphorn gespielt werden`);
    $('#result').removeClass();
    $('#result').addClass('answer_correct');
    $(button).addClass('button_correct');
  }
}

$(document).ready(function () {
  //  cache DOM elements
  //  const mainContent = $('#main-content');
  const header = $('#header');
  const sidebar = $('#sidebar-nav');
  const toggler = $('#sidebar-toggle');
  const sidebarHeight = sidebar.outerHeight();
  const contentHeight = $('#titlebar').outerHeight() + $('#main-content').outerHeight() + $('#footer').outerHeight();
  let scrolling = false;

  function checkScrollbarPosition() {
    const scrollTop = $(window).scrollTop();
    const headerHeight = header.outerHeight();
    if (headerHeight - scrollTop <= 0 && sidebarHeight < contentHeight) {
      sidebar.addClass('fixed-top-sidebar');
      toggler.addClass('fixed-top-sidebar');
    } else {
      sidebar.removeClass('fixed-top-sidebar');
      toggler.removeClass('fixed-top-sidebar');
    }
    scrolling = false;
  }

  $('#glossar-modal').on('loaded.modal', function (event) {
    window.location = `#${event.title}`;
  });

  checkScrollbarPosition();
  $(window).on('scroll', function () {
    if (!scrolling) {
      // eslint-disable-next-line
      (!window.requestAnimationFrame) ? setTimeout(checkScrollbarPosition, 300) :
      window.requestAnimationFrame(checkScrollbarPosition);
      scrolling = true;
    }
  });

  $('#hotspots div a').each(function () {
    if ($(this).attr('id') != null) {
      const audioElement = document.createElement('audio');
      const path = $(this).attr('id');
      audioElement.setAttribute('src', `/static/sounds/${path}.mp3`);
      audioElement.load();
      $(this).click(function () {
        audioElement.play();
      });
    }
  });

// eslint-disable-next-line
  var temperiert_nr_7 = document.createElement('audio');
  temperiert_nr_7.setAttribute('src', '/static/sounds/temperiert_nr_7.mp3');
  temperiert_nr_7.load();

// eslint-disable-next-line
  $('#temperiert_nr_7').click(function() {
    temperiert_nr_7.play();
  });

  // eslint-disable-next-line
  var naturton_nr_7 = document.createElement('audio');
  naturton_nr_7.setAttribute('src', '/static/sounds/naturton_nr_7.mp3');
  naturton_nr_7.load();

  // eslint-disable-next-line
  $('#naturton_nr_7').click(function() {
    naturton_nr_7.play();
  });

// eslint-disable-next-line
  var temperiert_nr_11 = document.createElement('audio');
  temperiert_nr_11.setAttribute('src', '/static/sounds/temperiert_nr_11.mp3');
  temperiert_nr_11.load();

// eslint-disable-next-line
  $('#temperiert_nr_11').click(function() {
    temperiert_nr_11.play();
  });

// eslint-disable-next-line
  var naturton_nr_11 = document.createElement('audio');
  naturton_nr_11.setAttribute('src', '/static/sounds/naturton_nr_11.mp3');
  naturton_nr_11.load();

// eslint-disable-next-line
  $('#naturton_nr_11').click(function() {
    naturton_nr_11.play();
  });

// eslint-disable-next-line
  $('#play_button_nr_7').click(function() {
    temperiert_nr_7.play();
    naturton_nr_7.play();
  });

// eslint-disable-next-line
  $('#play_button_nr_11').click(function() {
    temperiert_nr_11.play();
    naturton_nr_11.play();
  });
});
