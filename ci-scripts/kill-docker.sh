#!/bin/bash

# CI Script to stop and clean the docker container

# stop container
docker inspect -f  {{.State.Running}}  nmc_lms
ret=$?
if [ $ret -eq 0 ]; then
	docker ps -a
	# stop nmc_lms container
	docker stop nmc_lms
	docker ps -a
	# rm all non running containers
	docker rm `docker ps --no-trunc -aq`
	echo "docker container 'nmc_lms' killed"
fi

# remove image
docker inspect nmc_lms/dev-env > /dev/null
ret=$?
if [ $ret -eq 0 ]; then
	docker rmi nmc_lms/dev-env
	echo "docker image 'nmc_lms/dev-env' killed"
fi
