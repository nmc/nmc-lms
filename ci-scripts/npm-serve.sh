#!/bin/bash

# CI Script to run the server

/bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh \
        && mkvirtualenv nmc_lms \
        && workon nmc_lms \
        && npm run serve -- config.settings.test"
