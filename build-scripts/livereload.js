#! /usr/bin/env node

/*
* Run live reload from current project
*/

// require the module as normal
const bs = require('browser-sync').create();
const shell = require('shelljs');

// run the serve command first
shell.exec('npm run serve', { async: true });

// Listen to change events on HTML and reload
bs.watch(// remove parenthesis from
    'nmc_lms/**[/](*.js|*.map|*.src|*.css|*.html)',
    ).on('change', bs.reload);

bs.watch(// remove parenthesis from
    'src/**[/](*.js|*.scss)',
    ).on(
      'change', () => {
        shell.exec('npm run build', { silent: false });
      },
      );

bs.init({
  notify: false,
  proxy: `localhost: ${process.env.npm_package_config_port}`,
});

// run the serve command first
shell.exec('npm run serve&');
