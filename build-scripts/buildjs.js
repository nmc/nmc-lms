#! /usr/bin/env node

/*
* Add the steps to take to create deployable js files. I.e. minify, uglify,
* union, copy, ...
* As an example, you'll find the steps we took for the eEducation project
*/

const sh = require('shelljs');

const pathBase = 'project/lms_base/src/js/';
const pathNahtkurs = 'project/lms_nahtkurs/src/js/';
const pathOpos = 'project/lms_opos/src/js/';
const pathTales = 'project/lms_tales/src/js/';


const pathStatic = 'project/static/';

if (!sh.test('-e', pathBase)) {
  sh.mkdir('-p', pathBase);
}

if (!sh.test('-e', pathNahtkurs)) {
  sh.mkdir('-p', pathNahtkurs);
}

if (!sh.test('-e', pathOpos)) {
  sh.mkdir('-p', pathOpos);
}

if (!sh.test('-e', pathTales)) {
  sh.mkdir('-p', pathTales);
}


sh.cat(['project/lms_base/src/js/*.*']).to(`${pathStatic}lms_base/js/lms_base.js`);
sh.cat(['project/lms_nahtkurs/src/js/lms_nahtkurs.js']).to(`${pathStatic}lms_nahtkurs/js/lms_nahtkurs.js`);
sh.cat(['project/lms_nahtkurs/src/js/lms_nahtkurs_add.js']).to(`${pathStatic}lms_nahtkurs/js/lms_nahtkurs_add.js`);
sh.cat(['project/lms_nahtkurs/src/js/lms_nahtkurs_chapter_home.js']).to(`${pathStatic}lms_nahtkurs/js/lms_nahtkurs_chapter_home.js`);
sh.cat(['project/lms_nahtkurs/src/js/lms_nahtkurs_course_home.js']).to(`${pathStatic}lms_nahtkurs/js/lms_nahtkurs_course_home.js`);
sh.cat(['project/lms_opos/src/js/lms_opos.js']).to(`${pathStatic}lms_opos/js/lms_opos.js`);
sh.cat(['project/lms_opos/src/js/lms_opos_add.js']).to(`${pathStatic}lms_opos/js/lms_opos_add.js`);
sh.cat(['project/lms_opos/src/js/lms_opos_chapter_home.js']).to(`${pathStatic}lms_opos/js/lms_opos_chapter_home.js`);
sh.cat(['project/lms_opos/src/js/lms_opos_course_home.js']).to(`${pathStatic}lms_opos/js/lms_opos_course_home.js`);
sh.cat(['project/lms_tales/src/js/lms_tales.js']).to(`${pathStatic}lms_tales/js/lms_tales.js`);
sh.cat(['project/lms_tales/src/js/lms_tales_add.js']).to(`${pathStatic}lms_tales/js/lms_tales_add.js`);
sh.cat(['project/lms_tales/src/js/lms_tales_chapter_home.js']).to(`${pathStatic}lms_tales/js/lms_tales_chapter_home.js`);
sh.cat(['project/lms_tales/src/js/lms_tales_course_home.js']).to(`${pathStatic}lms_tales/js/lms_tales_course_home.js`);

/* copy vendor js files
if (!sh.test('-e', path + 'vendor')) {
  sh.mkdir('-p', path + 'vendor');
};

// jquery
sh.cp('node_modules/jquery/dist/jquery.min.js', path + 'vendor/');

// js files needed for bootstrap
sh.cp('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', path + 'vendor/');
*/
