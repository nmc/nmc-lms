#! /usr/bin/env node

/*
* Add the steps to take to create translated content As an example, you'll find
* the steps we took for the eEducation project

var shell = require("shelljs")


// make .po file
shell.exec("source " + process.env.WORKON_HOME + "/eEducation/bin/activate &&
cd eEducation && python manage.py makemessages --locale de");
// delete the line with POT-Creation-Date
shell.exec("source " + process.env.WORKON_HOME + "/eEducation/bin/activate &&
cd eEducation/locale/de/LC_MESSAGES/ && sed '/^\"POT-Creation-Date/'d django.po


> tmp && mv tmp django.po");
// make .mo binary file
shell.exec("source " + process.env.WORKON_HOME + "/eEducation/bin/activate &&
cd eEducation && python manage.py compilemessages -l de");
*/
