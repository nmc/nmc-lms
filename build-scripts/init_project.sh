#! /bin/bash
#
# initializes project in local django environment
pip install --upgrade pip
pip install virtualenv
pip install --upgrade virtualenv
virtualenv ~/.virtualenvs/nmc_lms
pip install --upgrade pip
pip install -r requirements/local.txt
npm install

# add lint to pre commit
cp git-hooks/pre-commit .git/hooks

# does not belong to initial process
# npm run build
# npm run serve
