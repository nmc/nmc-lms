#!/bin/bash

# code and site-packages seperated because it takes ages to uploade
# site-packages

# exit bash script if command failed
# doesnt work because davix returns 255 even in case of success
# set -e

USERNAME=""
PASSWORD=""

function usage {
	printf "usage: %s [--site-packages][--code]\n" $0
	exit 1
}

# is davix installed?
davix-ls -V 1>/dev/null

# max 1 parameter, must be --site-packages or --code
if [[ $# -gt 1 ]];
then
	echo "gt"
	usage
fi

case "$1" in
	--site-packages | --code | --nahtkurs | --opos)
		read -p 'Enter Username: ' USERNAME
		read -sp 'Enter Password: ' PASSWORD
		printf "\n\n"
		;;
	*)
		usage
		;;
esac

function upload_code {
	# first clean all files
	# ! media folder with Gaudenz' pics would be deleted!
	# davix-rm -r10 --userlogin $USERNAME --userpass $PASSWORD \
	# 	$npm_package_config_deploy_server_url/files/html

	# davix-rm -r10 --userlogin $USERNAME --userpass $PASSWORD \
	# 	$npm_package_config_deploy_server_url/files/cgi-bin/nmc-lms

	# recreate folder structure
	# davix-mkdir --userlogin $USERNAME --userpass $PASSWORD \
	#	$npm_package_config_deploy_server_url/files/html

	# davix-mkdir --userlogin $USERNAME --userpass $PASSWORD \
	# 	$npm_package_config_deploy_server_url/files/html/static

	# davix-mkdir --userlogin $USERNAME --userpass $PASSWORD \
	# 	$npm_package_config_deploy_server_url/files/cgi-bin/nmc-lms

	# prepare static files
	source $WORKON_HOME/nmc_lms/bin/activate \
		&& python project/manage.py collectstatic --noinput

	# upload!
	davix-put -r30 --userlogin $USERNAME --userpass $PASSWORD \
		./staticfiles \
		$npm_package_config_deploy_server_url/files/html/static/

	davix-put -r30 --userlogin $USERNAME --userpass $PASSWORD \
		./project \
		$npm_package_config_deploy_server_url/files/cgi-bin/nmc-lms/
}

function upload_libraries {
	# takes ages!

	# remove
  # Don't delete existing packages, just put new ones
	#davix-rm -r10 --userlogin $USERNAME --userpass $PASSWORD \
	#	$npm_package_config_deploy_server_url/files/cgi-bin/site-packages

	# upload
	davix-put -r30 --userlogin $USERNAME --userpass $PASSWORD \
		$WORKON_HOME/nmc_lms/lib/python2.7/site-packages \
		$npm_package_config_deploy_server_url/files/cgi-bin/site-packages
}

case "$1" in
	--site-packages)
		upload_libraries
		;;
	--code)
		upload_code
		;;
	*)
		echo "OOoop, you shouldnt see this!"
		;;
esac

# as said on top: davix always returns 255. turn exit code into 0:
exit 0
