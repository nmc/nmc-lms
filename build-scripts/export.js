#! /usr/bin/env node
const shell = require('shelljs');

/*
* Add code to export data from backend
*/

shell.exec('mkdir -p project/fixtures');
shell.exec('python project/manage.py dumpdata app --indent 2 > project/fixtures/fixtures.json');
