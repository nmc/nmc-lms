#!/bin/bash

# set django default environment
DJANGO_ENV='config.settings.local'
# if parameter passed, use it as environment
if [ -n "$1" ]; then
	DJANGO_ENV=$1;
fi

echo "using: $DJANGO_ENV"

# Apply missing migrations
source $WORKON_HOME/nmc_lms/bin/activate \
	&& python project/manage.py migrate

# Add test data to database
source $WORKON_HOME/nmc_lms/bin/activate \
	&& python project/manage.py test_data

# Start server
python project/manage.py runserver_plus 0.0.0.0:$npm_package_config_port --settings $DJANGO_ENV
# Somehow npm run serve exits with an error
# this is because the above 'manage.py runserver' command doesnt exit.
# this is solved by running it in background with '&' at the end of the line
# but then it doesnt work on the test server / demo server cannot start
#
exit 0; # overwrite error code of 'manage.py runserver'
