#! /usr/bin/env node
/* eslint comma-dangle: ["error", {"functions": "never"}] */
/* eslint no-console: ["error", { allow: ["log"] }] */
/* eslint func-names: ["error", "never"] */


/*
* Add the steps to take to create deployable css files. I.e. compile sass,
* minify, uglify, union, copy, ...
* As an example, you'll find the steps we took for the eEducation project
*/

const sh = require('shelljs');
const sass = require('node-sass');
const fs = require('fs');
const getDirName = require('path').dirname;

const pathStatic = 'project/static/';

/*
  Render base css
*/
sass.render(
  {
    file: 'project/lms_base/src/scss/lms_base_main.scss',
    includePaths: ['node_modules/bootstrap/scss'],
    outFile: 'project/static/lms_base/css/lms_base.css',
    outputStyle: 'compressed'
  },
  function (error, result) { // node-style callback from v3.0.0 onwards
    console.log(`=> render css to: ${this.options.outFile}`);
    if (error) {
      console.log(error.status); // used to be 'code' in v2x and below
      console.log(error.column);
      console.log(error.message);
      console.log(error.line);
    } else {
      if (!sh.test('-e', getDirName(this.options.outFile))) {
        sh.mkdir('-p', getDirName(this.options.outFile));
      }

      fs.writeFile(this.options.outFile, result.css, (err) => {
        if (err) throw err;
        console.log('SUCCESS');
      });
    }
  }
);

// Copy font files to static file folder

if (!sh.test('-e', `${pathStatic}lms_base/font`)) {
  sh.mkdir('-p', `${pathStatic}lms_base/font`);
}

sh.cp('project/lms_base/src/font/*.*', `${pathStatic}lms_base/font/`);

/*
  Render tales css
*/
sass.render(
  {
    file: 'project/lms_tales/src/scss/lms_tales_main.scss',
    includePaths: ['node_modules/bootstrap/scss'],
    outFile: 'project/static/lms_tales/css/lms_tales.css',
    outputStyle: 'compressed'
  },
  function (error, result) { // node-style callback from v3.0.0 onwards
    console.log(`=> render css to: ${this.options.outFile}`);
    if (error) {
      console.log(error.status); // used to be 'code' in v2x and below
      console.log(error.column);
      console.log(error.message);
      console.log(error.line);
    } else {
      if (!sh.test('-e', getDirName(this.options.outFile))) {
        sh.mkdir('-p', getDirName(this.options.outFile));
      }

      fs.writeFile(this.options.outFile, result.css, (err) => {
        if (err) throw err;
        console.log('SUCCESS');
      });
    }
  }
);

/*
  Render nahtkurs css
*/
sass.render(
  {
    file: 'project/lms_nahtkurs/src/scss/lms_nahtkurs_main.scss',
    includePaths: ['node_modules/bootstrap/scss'],
    outFile: 'project/static/lms_nahtkurs/css/lms_nahtkurs.css',
    outputStyle: 'compressed'
  },
  function (error, result) { // node-style callback from v3.0.0 onwards
    console.log(`=> render css to: ${this.options.outFile}`);
    if (error) {
      console.log(error.status); // used to be 'code' in v2x and below
      console.log(error.column);
      console.log(error.message);
      console.log(error.line);
    } else {
      if (!sh.test('-e', getDirName(this.options.outFile))) {
        sh.mkdir('-p', getDirName(this.options.outFile));
      }

      fs.writeFile(this.options.outFile, result.css, (err) => {
        if (err) throw err;
        console.log('SUCCESS');
      });
    }
  }
);

/*
  Render opos css
*/
sass.render(
  {
    file: 'project/lms_opos/src/scss/lms_opos_main.scss',
    includePaths: ['node_modules/bootstrap/scss'],
    outFile: 'project/static/lms_opos/css/lms_opos.css',
    outputStyle: 'compressed'
  },
  function (error, result) { // node-style callback from v3.0.0 onwards
    console.log(`=> render css to: ${this.options.outFile}`);
    if (error) {
      console.log(error.status); // used to be 'code' in v2x and below
      console.log(error.column);
      console.log(error.message);
      console.log(error.line);
    } else {
      if (!sh.test('-e', getDirName(this.options.outFile))) {
        sh.mkdir('-p', getDirName(this.options.outFile));
      }

      fs.writeFile(this.options.outFile, result.css, (err) => {
        if (err) throw err;
        console.log('SUCCESS');
      });
    }
  }
);
