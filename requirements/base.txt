# Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design.
#
#  https://github.com/django/django

Django==1.10

# Python MySQL connector
#
#  MySQLdb is an interface to the popular MySQL database server for Python.
#
#  https://pypi.python.org/pypi/mysqlclient

mysqlclient>=1.3.14
# If this doesn't work use `LDFLAGS=-L/usr/local/opt/openssl/lib pip install mysqlclient`

# Mixins for Django's class-based views.
#
#  It includes access, form & miscellaneous mixins
#
#  https://github.com/brack3t/django-braces

django-braces

# The best way to have DRY Django forms.
#
#  The app provides a tag and filter that lets you quickly render forms in a
#  div format while providing an enormous amount of capability to configure
#  and control the rendered HTML.
#
#  https://github.com/django-crispy-forms/django-crispy-forms

django-crispy-forms


# django-floppyforms is an application that gives you full control of the output of forms rendering.
# https://django-floppyforms.readthedocs.io

django-floppyforms

#select2
#The application includes Select2 driven Django Widgets and Form Fields.

django_select2

# I18N
#
#   Translates Django models using a registration approach.
#
#  https://github.com/deschler/django-modeltranslation

django-modeltranslation==0.12.1

# Model utilities
#
#  Django model mixins and utilities.
#
#  https://github.com/carljm/django-model-utils

django-model-utils

# Model version control
#
#  Store model history and view/revert changes from admin site.
#
#  https://github.com/treyhunner/django-simple-history

django-simple-history

# Images
#
#  The friendly PIL fork (Python Imaging Library) http://python-pillow.org
#
#  https://github.com/python-pillow/Pillow/

Pillow

# Automated image processing for Django.
#
#  https://github.com/matthewwithanm/django-imagekit

django-imagekit

# Time zones support
#
#  World timezone definitions, modern and historical
#
#  https://pypi.python.org/pypi/pytz/

pytz

# Settings
# unipath # important if we use python older v3.4 otherwhise use pathlib2

# User registration, either via email or social
#
#  Integrated set of Django applications addressing authentication,
#  registration, account management as well as 3rd party (social)
#  account authentication.
#
#  https://github.com/pennersr/django-allauth

django-allauth==0.33.0

# do ldap authentification
# repo: https://bitbucket.org/psagers/django-auth-ldap/overview
# docu: http://pythonhosted.org/django-auth-ldap/

python-ldap==2.4.41
# If this doesn't work use `LDFLAGS=-L/usr/local/opt/openssl/lib pip install python-ldap`
# Or:
# pip install python-ldap==2.5.2 \
#   --global-option=build_ext \
#   --global-option="-I$(xcrun --show-sdk-path)/usr/include/sasl"

django-auth-ldap==1.2.15

# Django UUSlug
#
#  A Django slugify application that guarantees Uniqueness and handles Unicode
#
# https://github.com/un33k/django-uuslug

django-uuslug==1.1.8


# Enable markdown support in admin
#
#  django-markdownx is a Markdown editor built for Django
#
# https://github.com/adi-/django-markdownx
# https://pyembed.github.io/usage/markdown/

django-markdownx==2.0.21
python-markdown-oembed==0.2.1



# Add system to allow object level permissions
#
#  Awesome Django authorization, without the database
#
# https://github.com/dfunckt/django-rules

rules


# Class-based generic views for Django
#
# django-extra-views
#
# repo: https://github.com/AndrewIngram/django-extra-views
# doc: https://django-extra-views.readthedocs.io/en/latest/

django-extra-views==0.9.0

# pyparsing
# https://pypi.python.org/pypi/pyparsing/1.5.7

pyparsing


python-memcached
