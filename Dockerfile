FROM mysql

VOLUME ["/etc/mysql", "/var/lib/mysql"]
ADD tales.sql /tmp/tales.sql
ADD db_init.sh /tmp/db_init.sh

ENV MYSQL_ROOT_PASSWORD seif3bieBohng8Vi
ENV MYSQL_DATABASE tales_nmc_unibas_ch_01

RUN chmod +x /tmp/db_init.sh
RUN /tmp/db_init.sh
